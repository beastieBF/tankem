## -*- coding: utf-8 -*-
from __future__ import division
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import webbrowser

#Dossier interfaceMenuSelectionNiveau
from interfaceMenuSelectionNiveau import *

import sys


class InterfaceMenuConnection(ShowBase):
    def __init__(self, dto, dtoListeNiveau, valeurNiveau, dtoUser, daoUser):

        self.p1Username = None
        self.p2Username = None
        self.users = dtoUser
        self.daoUser = daoUser

        self.valeur = valeurNiveau

        self.dto = dto
        
        self.dtoListeNiveau = dtoListeNiveau

        self.tank1DejaCharge = False
        self.tank2DejaCharge = False

        #Image d'arrière plan
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menuSelection.png")

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        #Titre du jeu
        self.textTitre = OnscreenText(text = "Connection des joueurs !",
                                      pos = (0,0.9), 
                                      scale = 0.1,
                                      frame = (0,0,0,1),
                                      bg = (0,0,0,0.75),
                                      fg=(0.8,0.9,0.7,1),
                                      align=TextNode.ACenter)

        self.textNomUsager1 = OnscreenText(text = "Nom d'usager du joueur 1",
                        pos = (-1,0.7), 
                        scale = 0.1,
                        frame = (0,0,0,1),
                        bg = (0,0,0,0.75),
                        fg=(0.8,0.9,0.7,1),
                        align=TextNode.ACenter)

        self.textNomUsager2 = OnscreenText(text = "Nom d'usager du joueur 2",
                                pos = (1,0.7), 
                                scale = 0.1,
                                frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)

        ##DIRECT ENTRY
        self.textObject = OnscreenText(text = "", 
                                pos = (0.95,-0.95), 
                                scale = 0.07,
                                fg=(1,0.5,0.5,1),
                                align=TextNode.ACenter,
                                mayChange=1)
        
        #callback function to set  text 
        def setText(textEntered):
                self.textObject.setText(textEntered)
        
        #add button
        self.joueur1Username = DirectEntry(text = "" ,
                        pos = (-1.57,0,0.58),
                        text_scale = (1.5,1.5),
                        scale=.05,
                        width = 15.2,
                        command=self.saveP1Username,
                        #focusOutCommand =self.saveP1Username,
                        #focusOutExtraArgs=[text],
                        #initialText="Xx_NoScopeGod_xX", 
                        numLines = 1,
                        focus=1)
        
        self.joueur2Username = DirectEntry(text = "" ,
                        pos = (0.42,0,0.58),
                        text_scale = (1.5,1.5),
                        scale=.05,
                        width = 15.2,
                        command=self.saveP2Username,
                        #focusOutCommand = self.saveP2Username,
                        #focusOutExtraArgs=[text],
                        #initialText="420Tanker", 
                        numLines = 1,
                        focus=0)

        self.joueur1Password=None
        self.joueur2Password=None

        
        #Création du message Connection
        self.messageConnection = ""

        self.setChampMessage(self.messageConnection)

        
        if self.dtoListeNiveau:
            self.myFrame.hide()

                #Boutons
        self.btnScale = (0.1,0.1)
        self.text_scale = 0.06
        self.borderW = (0.04, 0.04)
        self.couleurBack = (1,0,0,1)
        self.separation = 0.5
        self.hauteur = -0.85


        self.b2 = DirectButton(text = ("Site web", "Bye!", ":-)", "disabled"),
                          text_scale= 0.06,
                          borderWidth = self.borderW,
                          text_bg=(0,0.5,0.5,1),
                          frameColor=(0,0.5,0.5,1),
                          relief=2,
                          command = lambda : webbrowser.open('http://localhost/tankemwebsite/web/'),
                          pos = (1,0,self.hauteur))
       
    def cacher(self):
            #Est esssentiellement un code de "loading"

            #On remet la caméra comme avant
            base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
            #On cache les menus
            self.background.hide()
            self.b1.hide()
            self.b2.hide()
            self.textTitre.hide()
            self.textChampMessage.hide()
            self.joueur1Password.hide()
            self.joueur1Username.hide()
            self.joueur2Password.hide()
            self.joueur2Username.hide()
            self.myFrame.hide()
            self.textPassword1.hide()
            self.textPassword2.hide()
            self.textObject.hide()
            self.textNomUsager1.hide()
            self.textNomUsager2.hide() 
            self.myFrameVS.hide() 
            self.textJoueur1Nom.hide() 
            self.textJoueur2Nom.hide() 
            self.textVS.hide() 
            self.myFrameNiveau.hide() 
            self.textNomMap.hide() 
            self.myFrameFavori.hide() 
            self.textNomFavori.hide() 
            self.modeleTank1.hide()
            self.modeleTank2.hide()

    def chargeJeu(self, valeur):
            #On démarre!
            Sequence(#Func(lambda : self.transition.irisOut(0.2)),
                     #SoundInterval(self.sound),
                     Func(self.cacher),
                     Func(lambda : messenger.send("DemarrerPartie")),
                     #Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                     #Func(lambda : self.transition.irisIn(0.2))
            ).start()
    
    def chargerNiveau(self,valeur):
            Sequence(#Func(lambda : self.transition.irisOut(0.2)),
                     #SoundInterval(self.sound),
                     Func(self.cacher),
                     Func(lambda : messenger.send("chargerNiveau", [valeur])),
                     #Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                     #Func(lambda : self.transition.irisIn(0.2))
            ).start()

    def saveP1Username(self, username):
        self.p1Username = username
        if self.joueur1Password == None:
            self.textPassword1 = OnscreenText(text = "Mot de passe",
                        pos = (-1.36,0.47), 
                        scale = 0.07,
                        frame = (0,0,0,1),
                        bg = (0,0,0,0.75),
                        fg=(0.8,0.9,0.7,1),
                        align=TextNode.ACenter)

            self.joueur1Password = DirectEntry(text = "" ,
                            pos = (-1.14,0,0.47),
                            text_scale = (1.3,1.3),
                            scale=.04,
                            width = 13.5,
                            command=self.validateUser,
                            obscured = 1,
                            numLines = 1,
                            focus=1,
                            extraArgs = ["player1"])

    def saveP2Username(self, username):
        self.p2Username = username
        if self.joueur2Password == None:
            self.textPassword2 = OnscreenText(text = "Mot de passe",
                        pos = (0.63,0.47), 
                        scale = 0.07,
                        frame = (0,0,0,1),
                        bg = (0,0,0,0.75),
                        fg=(0.8,0.9,0.7,1),
                        align=TextNode.ACenter)

            self.joueur2Password = DirectEntry(text = "" ,
                        pos = (0.85,0,0.47),
                        text_scale = (1.3,1.3),
                        scale=.04,
                        width = 13.5,
                        command=self.validateUser,
                        obscured = 1,
                        numLines = 1,
                        focus=1,
                        extraArgs = ["player2"])

    def validateUser(self, passwd, player):
        username = None
        self.textChampMessage.hide()
        self.myFrame.hide()
        if(player == "player1"):
            username = self.p1Username
        else:
            username = self.p2Username
        
        response = self.daoUser.authenticate(username, passwd)
        
        if response != True:
            self.messageConnection = response
            self.setChampMessage(self.messageConnection)
        else:
            if player == "player1":
                if self.users["player1"].username == "" and self.users["player2"].username == "" :
                    self.messageConnection = "Connection réussi"
                    self.setChampMessage(self.messageConnection)
                    self.users["player1"] = self.daoUser.getPlayer(username)
                    self.chargerModeleTank(self.users)
                elif username != self.users["player2"].username:
                    self.messageConnection = "Connection réussi"
                    self.setChampMessage(self.messageConnection)
                    self.users["player1"] = self.daoUser.getPlayer(username)
                    self.chargerModeleTank(self.users)
                else:
                    self.setChampMessage("Utilisez deux usager différents D: !")
            else:
                if self.users["player1"].username == "" and self.users["player2"].username == "" :
                    self.messageConnection = "Connection réussi"
                    self.setChampMessage(self.messageConnection)
                    self.users["player2"] = self.daoUser.getPlayer(username)
                    self.chargerModeleTank(self.users)
                if self.users["player1"].username != username:
                    self.messageConnection = "Connection réussi"
                    self.setChampMessage(self.messageConnection)
                    self.users["player2"] = self.daoUser.getPlayer(username)
                    self.chargerModeleTank(self.users)
                else:
                    self.setChampMessage("Utilisez deux usager différents D: !")


            
            
            if self.tank1DejaCharge and self.tank2DejaCharge:
                self.deuxJoueurAuthentifier(self.users)


    def setChampMessage(self, messageConnection):

        self.myFrame = DirectFrame(  frameColor = (0.8,0.9,0.7,0.5),
                        frameSize=(-2, 2, -0.05, 0.1),
                        pos = (0,0,0.3), )
        
        if self.dtoListeNiveau is None:
            self.textChampMessage = OnscreenText(text = "Erreur de connection",
                                pos = (0,0.3), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(1,0,0,1),
                                align=TextNode.ACenter)
        elif self.messageConnection != "":
            self.textChampMessage = OnscreenText(text = messageConnection,
                                pos = (0,0.3), 
                                scale = 0.075,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(1,0,0,1),
                                align=TextNode.ACenter)
        elif self.messageConnection == "Connection réussi":
            self.textChampMessage = OnscreenText(text = messageConnection,
                                pos = (0,0.3), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)
        else:
            self.textChampMessage = OnscreenText(text = "",
                                pos = (0,0.3), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)

    def chargerModeleTank(self, users):
        if self.p1Username != None and self.tank1DejaCharge == False:
            # On charge le modèles
            self.tank1DejaCharge = True
            self.modeleTank1 = loader.loadModel("../asset/Tank/tank")

            self.modeleTank1.setScale(2,2,2)
            #On multiple la couleur de la texture du tank par ce facteur. Ça permet de modifier la couleur de la texture du tank
            self.modeleTank1.setColorScale(0,1,0,1) #get la couleur dans la bd

            self.modeleTank1.setHpr(220, 350, 0)

            self.modeleTank1.setPos(-8, 42, -2)

            hexa = users["player1"].couleurTank.lstrip('#')
            rgb = tuple(int(hexa[i:i+2], 16) for i in (0, 2 ,4))

            self.modeleTank1.setColorScale((rgb[0]/255), (rgb[1]/255), (rgb[2]/255),1)

            self.modeleTank1.reparentTo(base.render)

            #Effet de mouvement tank1
            self.intervalDeplacementJ1 = LerpPosInterval(self.modeleTank1, 1, (-8, 42, -2), (-20, 42, -2))
            self.intervalRotationJ1 = LerpHprInterval(self.modeleTank1, 4, (220, 350, 0), (-140, 350, 0))
            self.sequenceDeplacementJ1 = Sequence(self.intervalDeplacementJ1
                                                , Func(lambda : self.intervalRotationJ1.loop()))
            self.sequenceDeplacementJ1.start()
        
        if self.p2Username != None and self.tank2DejaCharge == False:
            ############################DEUXIEME TANK
            self.tank2DejaCharge = True
            self.modeleTank2 = loader.loadModel("../asset/Tank/tank")

            self.modeleTank2.setScale(2,2,2)

            self.modeleTank2.setColorScale(0,0,1,1) #get la couleur dans la bd

            self.modeleTank2.setHpr(140, 350, 0)

            self.modeleTank2.setPos(8, 42, -2)

            hexa = users["player2"].couleurTank.lstrip('#')
            rgb = tuple(int(hexa[i:i+2], 16) for i in (0, 2 ,4))

            self.modeleTank2.setColorScale((rgb[0]/255), (rgb[1]/255), (rgb[2]/255),1)

            self.modeleTank2.reparentTo(base.render)

            self.intervalDeplacementJ2 = LerpPosInterval(self.modeleTank2, 1, (8, 42, -2), (20, 42, -2))
            self.intervalRotationJ2 = LerpHprInterval(self.modeleTank2, 4, (140, 350, 0), (500, 350, 0))
            self.sequenceDeplacementJ2 = Sequence(self.intervalDeplacementJ2
                                                , Func(lambda : self.intervalRotationJ2.loop()))
            self.sequenceDeplacementJ2.start()

    def deuxJoueurAuthentifier(self, users):
        self.myFrameVS = DirectFrame(  frameColor = (0.8,0.9,0.7,0.5),
                                frameSize=(-1, 1, -0.05, 0.5),
                                pos = (0,0,-0.3), )

        usernameP1A= ""
        usernameP1B= ""
        if users["player1"].qualificatifA != None:
            usernameP1A = users["player1"].qualificatifA
        if users["player1"].qualificatifB != None:
            usernameP1B = users["player1"].qualificatifB
        
        usernameP2A= ""
        usernameP2B= ""
        if users["player2"].qualificatifA != None:
            usernameP2A = users["player2"].qualificatifA
        if users["player2"].qualificatifB != None:
            usernameP2B = users["player2"].qualificatifB
        

        self.textJoueur1Nom = OnscreenText(text = users["player1"].username + " " + usernameP1A + " " + usernameP1B , #plugger les nom d'usager une fois connecter
                                pos = (0,0.05), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)

        self.textJoueur2Nom = OnscreenText(text = users["player2"].username + " " + usernameP2A + " " + usernameP2B , #plugger les nom d'usager une fois connecter
                                pos = (0,-0.25), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)
        
        self.textVS = OnscreenText(text = "VS",
                                pos = (0,-0.1), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)

        self.animationFrame = LerpScaleInterval(self.myFrameVS, 1, 1, 0)
        self.animationText1 = LerpScaleInterval(self.textJoueur1Nom, 1, 1, 0)
        self.animationText2 = LerpScaleInterval(self.textJoueur2Nom, 1, 1, 0)
        self.animationText3 = LerpScaleInterval(self.textVS, 1, 1, 0)
        
        self.animationFrame.start()
        self.animationText1.start()
        self.animationText2.start()
        self.animationText3.start()

        #################################################

        self.myFrameNiveau = DirectFrame(  frameColor = (0.8,0.9,0.7,0.5),
                                frameSize=(-0.5, 0.5, -0.05, 0.2),
                                pos = (-1,0,-0.6), )

        if self.valeur == "-1":
            self.nomNiveau = "Map aléatoire"
        else:
            self.nomNiveau = self.valeur
        self.textNomMap = OnscreenText(text = "Combattrons dans : \n" + self.nomNiveau, 
                                pos = (-1,-0.5), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)


        self.myFrameFavori = DirectFrame(  frameColor = (0.8,0.9,0.7,0.5),
                                frameSize=(-0.5, 0.5, -0.05, 0.2),
                                pos = (1,0,-0.6), )
        
        #user avec le plus haut niveau
        
        if users["player1"].niveau > users["player2"].niveau:
            joueurFavoris = users["player1"].username
        elif users["player1"].niveau < users["player2"].niveau:
            joueurFavoris = users["player2"].username
        else:
            joueurFavoris = "Aucun joueur"
        
        self.textNomFavori = OnscreenText(text = joueurFavoris + "\n favorisé",  #chercher le joueur avec le plus haut niveau dans la bd
                                pos = (1,-0.5), 
                                scale = 0.1,
                                #frame = (0,0,0,1),
                                bg = (0,0,0,0.75),
                                fg=(0.8,0.9,0.7,1),
                                align=TextNode.ACenter)

        if self.valeur == "-1":
            self.commandBouton = self.chargeJeu
        else:
            self.commandBouton = self.chargerNiveau

        #if les deux joueur sont connecté
        self.b1 = DirectButton(text = ("COMBATTRE !", "!", "!", "disabled"),
                          text_scale=self.btnScale,
                          borderWidth = self.borderW,
                          text_bg=self.couleurBack,
                          frameColor=self.couleurBack,
                          relief=2,
                          command = self.animationStart,
                          pos = (0,0,self.hauteur),
                          extraArgs= [self.valeur])

        #Effet de mouvement boutons
        self.intervalGrossissement = LerpScaleInterval(self.b1, 0.4, 1.4, 1.0)
        self.intervalRapetissement = LerpScaleInterval(self.b1, 0.4, 1, 1.4)
        self.sequenceMouvement = Sequence(self.intervalGrossissement
                                ,self.intervalRapetissement)
        
        self.sequenceMouvement.loop()

    def animationStart(self, valeur):
        # if self.commandBouton == self.chargeJeu:
        #     self.charger = self.chargeJeu(valeur)
        # if self.commandBouton == self.chargerNiveau:
        #     self.charger = self.chargerNiveau(valeur)

        self.intervalAnimationStartJ1 = LerpPosInterval(self.modeleTank1, 5, self.modeleTank1.getPos() - Vec3(0,70,100) ,self.modeleTank1.getPos() )
        self.intervalAnimationStartJ2 = LerpPosInterval(self.modeleTank2, 5, self.modeleTank2.getPos() - Vec3(0,70,100) ,self.modeleTank2.getPos() )
        self.intervalAnimationStartJ1.start()
        self.intervalAnimationStartJ2.start()
        self.sequenceAnimationStartJ1 = Sequence(Wait(2)
                                            ,Func(lambda : self.chargeJeu(valeur) if self.commandBouton == self.chargeJeu else self.chargerNiveau(valeur))
                                            )
        self.sequenceAnimationStartJ1.start()

        