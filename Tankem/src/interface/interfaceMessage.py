## -*- coding: utf-8 -*-
from util import *

import sys
import webbrowser
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
import math

sys.path.insert(0, '../../../Common/Internal')
from DatabaseManager import *

# from direct.filter.CommonFilters import CommonFilters

 
class InterfaceMessage(ShowBase):
    def __init__(self, dto , dtoNiveau):
        
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)

        self.dto = dto

        self.dtoNiveau = dtoNiveau

        self.idGagnant = None
        self.idPerdant = None

        self.accept("tankGagnant",self.setIdGagnant)
        self.accept("tankElimine",self.setIdPerdant)
        self.accept("finPartie",self.displayGameOver)

        self.callBackFunction = None

        self.accept("showHelp",self.displayHelp)

        self.createHelpText()
        self.displayHelp(False)

    def effectCountDownStart(self,nombre,callbackFunction):
        self.callBackFunction = callbackFunction
        self.displayCountDown(nombre)
        
    def displayCountDown(self, nombre):
        message = str(nombre)
        startScale = 0.4

        text = TextNode('Compte à rebour')
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.5, 0.5, 1, 1)
        text.setAlign(TextNode.ACenter)

        effetScale = LerpScaleInterval(textNodePath, 1.0, 0.05, startScale)
        effetFadeOut = LerpColorScaleInterval(textNodePath, 1.0, LVecBase4(1,1,1,0), LVecBase4(1,1,1,1))
        effetFadeOut.start()

        recursion = Func(self.displayCountDown,nombre-1)

        #Le prochain tour, on affiche la message de début de partie
        if(nombre == 1):
            recursion = Func(self.displayStartGame)
        sequence = Sequence(effetScale,recursion)
        sequence.start()

    def displayStartGame(self):
        message = self.dto.game_start_message # Valeur a chercher dans la BD
        startScale = 0.4

        text = TextNode('Début de la partie')
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.5, 0.5, 1, 1)
        text.setAlign(TextNode.ACenter)

        delai = Wait(self.dto.welcome_message_time) # Valeur a chercher dans la BD
        effetFadeOut = LerpColorScaleInterval(textNodePath, 0.15, LVecBase4(1,1,1,0), LVecBase4(1,1,1,1), blendType = 'easeIn')

        sequence = Sequence(delai,effetFadeOut,Func(self.callBackFunction))
        sequence.start()

    def setIdPerdant(self, idPerdant, dtoStatsPerdant, dtoUsagerPerdant) :
        self.idPerdant = idPerdant
        self.dtoStatsPerdant = dtoStatsPerdant
        self.dtoUsagerPerdant = dtoUsagerPerdant
        self.verifDataFinPartie()

    def setIdGagnant(self, idWinner, dtoStatsGagnant, vieGagnantRestant, vieGagnantTotal, dtoUsagerGagnant):
        self.idGagnant = idWinner
        self.dtoStatsGagnant = dtoStatsGagnant
        self.vieGagnantRestant = vieGagnantRestant
        self.vieGagnantPerdu = vieGagnantTotal - vieGagnantRestant
        self.dtoUsagerGagnant = dtoUsagerGagnant
        self.verifDataFinPartie()

    def verifDataFinPartie(self):
        if self.idGagnant != None and self.idPerdant != None :
            messenger.send("finPartie")


    def displayGameOver(self):

        # Création des DAOs
        daoStatsPartie = DaoStatsPartieOracle()
        daoStatsJoueur = DaoJoueurPartieOracle()
        analyse = DaoAnalyseStats()
        daoUsager = DaoUser()

        #Création de l'objet dtoStatsPartie
        self.dtoStatsPartie = DTOStatsPartie()
        self.dtoStatsPartie.id_niveau = self.dtoNiveau.id
        self.dtoStatsPartie.id_gagnant = self.dtoUsagerGagnant.idUser

        #Insertion dans la BD et retour du id de l'insertion pour les dtoStatsJoueur
        daoStatsPartie.id = daoStatsPartie.write( self.dtoStatsPartie )
        self.dtoStatsPerdant.id_stats_partie = daoStatsPartie.id
        self.dtoStatsGagnant.id_stats_partie = daoStatsPartie.id

        # Calcul de XP
        # On détermine le favori
        favori = False
        if self.dtoUsagerPerdant.niveau < self.dtoUsagerGagnant.niveau :
            favori = True

        # On calcule le XP
        xpGagnant = 100 + self.vieGagnantRestant*2
        if not favori :
            xpGagnant += 100
        self.dtoStatsGagnant.experience = xpGagnant

        xpPerdant = self.vieGagnantPerdu*2
        self.dtoStatsPerdant.experience = xpPerdant

        # On insére les dto StatsJoueur
        daoStatsJoueur.write(self.dtoStatsGagnant)
        daoStatsJoueur.write(self.dtoStatsPerdant)

        # On calcule le xp total
        self.dtoUsagerGagnant.experience += xpGagnant
        self.dtoUsagerPerdant.experience += xpPerdant

        # On calcule le seuil pour le level up
        seuilNiveauSuivantGagnant = 100 * (self.dtoUsagerGagnant.niveau + 1) + 50 * math.pow(self.dtoUsagerGagnant.niveau,2)
        seuilNiveauSuivantPerdant = 100 * (self.dtoUsagerPerdant.niveau + 1) + 50 * math.pow(self.dtoUsagerPerdant.niveau,2)

        # On vérifie s'il y a un level up et on augmente le level
        levelUpGagnant = False      # Boolean pour declencher une animation
        levelUpPerdant = False

        if self.dtoUsagerGagnant.experience >= seuilNiveauSuivantGagnant :
            levelUpGagnant = True
            self.dtoUsagerGagnant.niveau += 1
            self.dtoUsagerGagnant.point_attribut += 5

        if self.dtoUsagerPerdant.experience >= seuilNiveauSuivantPerdant :
            levelUpPerdant = True
            self.dtoUsagerPerdant.niveau += 1
            self.dtoUsagerPerdant.point_attribut += 5

        # Calcul du ratio victoire/defaite
        # Gagnant
        totalGame = analyse.getTotalGamePlayed(self.dtoUsagerGagnant.idUser)
        totalWinGame = analyse.getWinGamePlayed(self.dtoUsagerGagnant.idUser)
        totalLostGame = totalGame-totalWinGame
        if totalLostGame == 0 or totalGame == 0:
            self.dtoUsagerGagnant.winRate = 0
        else :
            self.dtoUsagerGagnant.winRate = totalWinGame / totalLostGame

        # Perdant
        totalGame = analyse.getTotalGamePlayed(self.dtoUsagerPerdant.idUser)
        totalWinGame = analyse.getWinGamePlayed(self.dtoUsagerPerdant.idUser)
        totalLostGame = totalGame-totalWinGame
        
        if totalLostGame == 0 or totalGame == 0:
            self.dtoUsagerPerdant.winRate = 0
        else :
            self.dtoUsagerPerdant.winRate = totalWinGame / totalLostGame

        # Recherche de la map favorite
        # Gagnant
        idMap = analyse.getFavoriteMapId(self.dtoUsagerGagnant.idUser)
        self.dtoUsagerGagnant.bestMap = analyse.getMapNameById(idMap)
        # Perdant
        idMap = analyse.getFavoriteMapId(self.dtoUsagerPerdant.idUser)
        self.dtoUsagerPerdant.bestMap = analyse.getMapNameById(idMap)

        # Recherche des armes favorites
        # Gagnant
        listFavoriteWeapon = analyse.getFavoriteWeapons(self.dtoUsagerGagnant.idUser)
        tupleArme1 = listFavoriteWeapon[0]
        tupleArme2 = listFavoriteWeapon[1]
        self.dtoUsagerGagnant.arme1 = tupleArme1[1]
        self.dtoUsagerGagnant.arme2 = tupleArme2[1]
        # Perdant
        listFavoriteWeapon = analyse.getFavoriteWeapons(self.dtoUsagerPerdant.idUser)
        tupleArme1 = listFavoriteWeapon[0]
        tupleArme2 = listFavoriteWeapon[1]
        self.dtoUsagerPerdant.arme1 = tupleArme1[1]
        self.dtoUsagerPerdant.arme2 = tupleArme2[1]

        # On insère les DTOs des usagers
        daoUsager.updatePlayerEndGame(self.dtoUsagerGagnant)
        daoUsager.updatePlayerEndGame(self.dtoUsagerPerdant)
        
        # Fin de l'analyse
        # ################################################

        joueurGagnant = 1 if self.idPerdant == 1 else 2
        self.dto.endGameMessageCreation(joueurGagnant) 
        message = self.dto.game_over_message # Valeur a chercher dans la BD
        startScale = 0.2

        text = TextNode('Annonce game over')
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        textNodePath.setColorScale(LVecBase4(1,1,1,0))
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.01, 0.2, 0.7, 1)
        text.setAlign(TextNode.ACenter)

        delai = Wait(0.5)
        effetFadeIn = LerpColorScaleInterval(textNodePath, 1, LVecBase4(1,1,1,1), LVecBase4(1,1,1,0), blendType = 'easeIn')
        effetFadeOut = LerpColorScaleInterval(textNodePath, 1, LVecBase4(1,1,1,0), LVecBase4(1,1,1,1), blendType = 'easeIn')

        sequence = Sequence(delai,effetFadeIn)
        sequence.start()

        delai = Wait(3)
        sequence = Sequence(delai,effetFadeOut)
        sequence.start()

        if self.idGagnant == 0 :
            # Gagnant
            nomJoueur1 = self.dtoUsagerGagnant.username
            xpFinalJoueur1 = self.dtoUsagerGagnant.experience
            xpPartieJoueur1 = self.dtoStatsGagnant.experience
            xpJoueur1 = xpFinalJoueur1 - xpPartieJoueur1
            stringCalculXPJoueur1 = str(xpJoueur1) + "xp + " + str(xpPartieJoueur1) + "xp = " + str(xpFinalJoueur1) + "xp"
            niveauJoueur1 = self.dtoUsagerGagnant.niveau
            
            # Gagnant
            nomJoueur2 = self.dtoUsagerPerdant.username
            xpFinalJoueur2 = self.dtoUsagerPerdant.experience
            xpPartieJoueur2 = self.dtoStatsPerdant.experience
            xpJoueur2 = xpFinalJoueur2 - xpPartieJoueur2
            stringCalculXPJoueur2 = str(xpJoueur2) + "xp + " + str(xpPartieJoueur2) + "xp = " + str(xpFinalJoueur2) + "xp"
            niveauJoueur2 = self.dtoUsagerPerdant.niveau
        else :
            # Gagnant
            nomJoueur2 = self.dtoUsagerGagnant.username
            xpFinalJoueur2 = self.dtoUsagerGagnant.experience
            xpPartieJoueur2 = self.dtoStatsGagnant.experience
            xpJoueur2 = xpFinalJoueur2 - xpPartieJoueur2
            stringCalculXPJoueur2 = str(xpJoueur2) + "xp + " + str(xpPartieJoueur2) + "xp = " + str(xpFinalJoueur2) + "xp"
            niveauJoueur2 = self.dtoUsagerGagnant.niveau
            
            # Gagnant
            nomJoueur1 = self.dtoUsagerPerdant.username
            xpFinalJoueur1 = self.dtoUsagerPerdant.experience
            xpPartieJoueur1 = self.dtoStatsPerdant.experience
            xpJoueur1 = xpFinalJoueur1 - xpPartieJoueur1
            stringCalculXPJoueur1 = str(xpJoueur1) + "xp + " + str(xpPartieJoueur1) + "xp = " + str(xpFinalJoueur1) + "xp"
            niveauJoueur1 = self.dtoUsagerPerdant.niveau

        # Noms des joueurs
        self.textJoueur1 = OnscreenText(text = nomJoueur1, # Valeur a chercher dans le DTO
                                      pos = (-1,0.6), 
                                      scale = 0.1,
                                      shadow = (0,0,0,1),
                                      fg = (0.8,0.9,0.7,1),
                                      align = TextNode.ACenter)

        self.textJoueur2 = OnscreenText(text = nomJoueur2, # Valeur a chercher dans le DTO
                                      pos = (1,0.6), 
                                      scale = 0.1,
                                      shadow = (0,0,0,1),
                                      fg = (0.8,0.9,0.7,1),
                                      align = TextNode.ACenter)

        self.textXPJoueur1 = OnscreenText(text = stringCalculXPJoueur1, # Valeur a chercher dans le DTO
                                      pos = (-1,-0.2), 
                                      scale = 0.1,
                                      shadow = (0,0,0,1),
                                      fg = (0.8,0.9,0.7,1),
                                      align = TextNode.ACenter)

        self.textXPJoueur2 = OnscreenText(text = stringCalculXPJoueur2, # Valeur a chercher dans le DTO
                                      pos = (1,-0.2), 
                                      scale = 0.1,
                                      shadow = (0,0,0,1),
                                      fg = (0.8,0.9,0.7,1),
                                      align = TextNode.ACenter)

        self.textNiveauJoueur1 = OnscreenText(text = "Niveau: " + str(niveauJoueur1), # Valeur a chercher dans le DTO
                                      pos = (-1,-0.4), 
                                      scale = 0.1,
                                      shadow = (0,0,0,1),
                                      fg = (0.8,0.9,0.7,1),
                                      align = TextNode.ACenter)

        self.textNiveauJoueur2 = OnscreenText(text = "Niveau: " + str(niveauJoueur2), # Valeur a chercher dans le DTO
                                      pos = (1,-0.4), 
                                      scale = 0.1,
                                      shadow = (0,0,0,1),
                                      fg = (0.8,0.9,0.7,1),
                                      align = TextNode.ACenter)

        # Boutons
        btnScale = (0.18,0.18)
        text_scale = 0.12
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 0.5
        hauteur = -0.8

        self.b1 = DirectButton(text = ("Stats", "!", "!", "disabled"),
                          text_scale = btnScale,
                          borderWidth = borderW,
                          text_bg = couleurBack,
                          frameColor = couleurBack,
                          relief = 2,
                          command = lambda : webbrowser.open('http://localhost/tankemwebsite/web/index.php?username=' + nomJoueur1 + ''),
                          pos = (-separation - separation, 0, hauteur))

        self.b2 = DirectButton(text = ("Stats", "!", "!", "disabled"),
                          text_scale = btnScale,
                          borderWidth = borderW,
                          text_bg = couleurBack,
                          frameColor = couleurBack,
                          relief = 2,
                          command = lambda : webbrowser.open('http://localhost/tankemwebsite/web/index.php?username=' + nomJoueur2 + ''),
                          pos = (separation * 2, 0, hauteur))

        self.b3 = DirectButton(text = ("Quitter", "Bye!", ":O", "disabled"),
                          text_scale = btnScale,
                          borderWidth = borderW,
                          text_bg = couleurBack,
                          frameColor = couleurBack,
                          relief = 2,
                          command = lambda : sys.exit(),
                          pos = (0, 0, hauteur))
        
        ########### TEST TANKS
        messenger.send("cameraFinPartie")

        self.modeleTank1 = loader.loadModel("../asset/Tank/tank")
        self.modeleTank1.setScale(1,1,1)
        #On multiple la couleur de la texture du tank par ce facteur. Ça permet de modifier la couleur de la texture du tank
        self.modeleTank1.setColorScale(0,1,0,1) #get la couleur dans la bd
        self.modeleTank1.setHpr(0, 0, 0)
        self.modeleTank1.setHpr(0, 0, 0)
        self.modeleTank1.setPos(0, 0, 0)
        hexa = self.dtoUsagerGagnant.couleurTank.lstrip('#')
        rgb = tuple(int(hexa[i:i+2], 16) for i in (0, 2 ,4))
        self.modeleTank1.setColorScale((rgb[0]/255), (rgb[1]/255), (rgb[2]/255),1)
        self.modeleTank1.reparentTo(base.render)
        self.intervalDeplacementJ1 = LerpPosInterval(self.modeleTank1, 1, (0, 22, 0), (0, 22, 20))
        self.intervalRotationJ1 = LerpHprInterval(self.modeleTank1, 4, (0, 0, 0), (360, 0, 0))
        self.sequenceDeplacementJ1 = Sequence(self.intervalDeplacementJ1 , Func(lambda : self.intervalRotationJ1.loop()) )
        self.sequenceDeplacementJ1.start()

    def effectMessageGeneral(self, message, duration):
        text = TextNode('Message general')

        startScale = 0.12
        text.setText(message)
        textNodePath = aspect2d.attachNewNode(text)
        textNodePath.setScale(startScale)
        textNodePath.setColorScale(LVecBase4(1,1,1,0))
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.01, 0.2, 0.7, 1)
        text.setAlign(TextNode.ACenter)
        textNodePath.setPos(Vec3(0,0,0.65))

        delai = Wait(duration)
        effetFadeIn = LerpColorScaleInterval(textNodePath, 0.3, LVecBase4(1,1,1,1), LVecBase4(1,1,1,0), blendType = 'easeIn')
        effetFadeOut = LerpColorScaleInterval(textNodePath, 0.3, LVecBase4(1,1,1,0), LVecBase4(1,1,1,1), blendType = 'easeIn')

        sequence = Sequence(effetFadeIn,delai,effetFadeOut)
        sequence.start()

    def displayHelp(self,mustShow):
        self.textNodePath.show() if mustShow else self.textNodePath.hide()

    def createHelpText(self):
        #TODO: touches sont dupliquées dans le inputManager et ici. On doit centraliser
        message = """Contrôle\n
        Contrôle avec la souris: F2\n
        ----Joueur 1----\n
        Bouger: wasd\n
        Tirer arme principale: v\n
        Tirer arme secondaire: b\n
        Détonation des balles: b\n
        \n
        ----Joueur 2----\n
        Bouger: flèches\n
        Tirer arme principale: NumPad-1\n
        Tirer arme secondaire: NumPad-2\n
        Détonation des balles: NumPad-3\n\n
        """

        text = TextNode('Aide')
        text.setText(message)
        self.textNodePath = aspect2d.attachNewNode(text)
        self.textNodePath.setScale(0.055)
        self.textNodePath.setColorScale(LVecBase4(1,1,1,1))
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        text.setTextColor(0.01, 0.2, 0.1, 1)
        text.setAlign(TextNode.ALeft)
        self.textNodePath.setPos(Vec3(-1.65,0,0.65))