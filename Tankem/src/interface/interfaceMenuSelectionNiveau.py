# -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions

from interfaceMenuConnection import *

import sys



class InterfaceMenuSelectionNiveau(ShowBase):
    def __init__(self, dto, dtoListeNiveau, dtoUser, daoUser, idNiveau = None ):
        
        self.dto = dto
        self.dtoListeNiveau = dtoListeNiveau
        self.idNiveau = idNiveau
        self.dtoUser = dtoUser
        self.daoUser = daoUser
        #Image d'arrière plan
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/LevelMenuBG.jpg")

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        #Titre du jeu
        self.textTitre = OnscreenText(text = "selection\n du\n niveau!", # Valeur a chercher dans le DTO
                                      pos = (-1,0.75), 
                                      scale = 0.3,
                                      shadow = (0,0,0,1),
                                      fg=(0.8,0.9,0.7,1),
                                      align=TextNode.ACenter)

        #Boutons
        btnScale = (0.1,0.1)
        text_scale = 0.12
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 1
        hauteur = -0.8
        self.b1 = DirectButton(text = ("Choisir Map Aleatoire", "!", "!", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          #command=self.chargeJeu,
                          command=self.chargeMenuConnection,
                          pos = (separation,0,hauteur),
                          #-1 est équivalent a l'id de la map aléatoire ici (I know....i know.)
                          extraArgs= ["-1"])
                          #peut etre besoin dune liste dans une lsite (attention!)


        self.b2 = DirectButton(text = ("Quitter", "Bye!", ":-(", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command = lambda : sys.exit(),
                          pos = (-separation -0.33,0,hauteur-0.1))
 
        self.numItemsVisible = 5
        self.itemHeight = 0.2
        
        self.myScrolledList = DirectScrolledList(
            decButton_pos= (0.35, 0, 0.53),
            decButton_text = "UP",
            decButton_text_scale = 0.08,
            decButton_borderWidth = (0.005, 0.005),
 
            incButton_pos= (0.35, 0, -0.53),
            incButton_text = "DOWN",
            incButton_text_scale = 0.08,
            incButton_borderWidth = (0.005, 0.005),
        
            frameSize = (-0.2, 0.9, -0.54, 0.59),
            frameColor = (0.8,0.9,0.7,0.5),
            pos = (0.65, 0, 0),
            numItemsVisible = self.numItemsVisible,
            forceHeight = self.itemHeight,
            #itemFrame_frameSize = (-0.2, 0.2, -0.5, 0.59),
            itemFrame_pos = (0.35, 0, 0.4),
            )
        self.textNoConnection = OnscreenText(text = "Erreur de connection\n à\n la base de donnée", # Valeur a chercher dans le DTO
                                    pos = (1,0.25), 
                                    scale = 0.1,
                                    frame = (0,128,0,0.5),
                                    bg = (0,128,0,0.5),
                                    shadow = (0,0,0,1),
                                    fg=(1,0,0,1),
                                    align=TextNode.ACenter)
        
        if self.dtoListeNiveau is None:
            self.myScrolledList.hide()
        else:
            self.textNoConnection.hide()
            self.liste = []

            self.liste = self.dtoListeNiveau.listeNiveau.keys()

            self.liste.sort()

            for i in range(0,len(self.liste)): #PLUGGER A LA BASE DE DONNÉ
                if idNiveau == self.liste[i]:
                    l = DirectButton(text = (self.liste[i], "click!", "erreur", "disabled"),
                                text_scale=btnScale,
                                #command = self.chargerNiveau,
                                borderWidth = borderW,
                                text_bg=(255,0,0,1),
                                frameColor=(255,0,0,1),
                                relief=2,
                                extraArgs= [self.liste[i]])
                else:
                    l = DirectButton(text = (self.liste[i], "click!", "roll", "disabled"),
                                text_scale=btnScale,
                                command = self.chargeMenuConnection,
                                borderWidth = borderW,
                                text_bg=couleurBack,
                                frameColor=couleurBack,
                                relief=2,
                                extraArgs= [self.liste[i]])
                self.myScrolledList.addItem(l) 

        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)
        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")
        self.accept("chargeMenu",self.chargeMenu)
    
    def cacher(self):
            #Est esssentiellement un code de "loading"

            #On remet la caméra comme avant
            base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
            #On cache les menus
            self.background.hide()
            self.b1.hide()
            self.b2.hide()
            self.textTitre.hide()
            self.myScrolledList.hide()
            self.textNoConnection.hide()
       

    
    def chargeMenuConnection(self, valeur):
            #On démarre!
            Sequence(#Func(lambda : self.transition.irisOut(0.2)),
                     #SoundInterval(self.sound),
                     Func(self.cacher),
                     Func(lambda : messenger.send("chargeMenu", [valeur])),
                     #Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                     #Func(lambda : self.transition.irisIn(0.2))
            ).start()

    def chargeMenu(self,valeur):
        self.menuConnection = InterfaceMenuConnection(self.dto, self.dtoListeNiveau, valeur, self.dtoUser, self.daoUser)


        
