# -*- coding: utf-8 -*-
from util import *

from direct.showbase.ShowBase import ShowBase
from panda3d.core import *
from panda3d.bullet import BulletWorld
from panda3d.bullet import BulletPlaneShape
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletDebugNode

import sys
sys.path.insert(0, '../../Common/Internal')

from DatabaseManager import *
#Modules de notre jeu
from map import Map
from inputManager import InputManager
from interface import *
import Tkinter as tk
import tkMessageBox
import time
import math

#Classe qui gère les phases du jeu (Menu, début, pause, fin de partie)
class GameLogic(ShowBase):
    def __init__(self,pandaBase):
        self.interface = InterfaceDAO()
        self.daoOracle = DaoBalanceOracle()
        self.daoListeNiveau = DaoListeNiveauOracle()
        self.daoNiveau = DaoNiveauOracle()
        self.daoListeJoueur = DaoListeJoueurOracle() 
        self.daoListeCase = DaoListeCaseOracle()
        self.daoEnregistrement = DaoEnrPartie()

        #Ce fait meme si il y a une connection a la BD pour l'instant
        # self.dtoListeNiveau = DTOListeNiveau()
        self.dtoNiveau = DTONiveau(0)
        #self.dtoCase = DTOCase()
        self.dtoJoueur1 = DTOJoueur(0)
        self.dtoJoueur2 = DTOJoueur(1)
        self.dtoPartie = DTOPartie()

        self.listecase = None
        self.sanitizerNiveau = SanitizerNiveau()
        self.sanitizerListeJoueur = SanitizerListeJoueur()


        if self.interface.isConnection() and not self.interface.databaseIntegrityCheck() :
            self.dto = self.daoOracle.read()
            self.dtoListeNiveau = self.daoListeNiveau.readAll()
        else:
            root = tk.Tk()
            root.withdraw()
            tkMessageBox.showwarning("Echec de connection","Les valeurs par défaut sont utilisées car la connection à la base de donnée n'a pas pu être établie.")
            root.deiconify()
            root.minsize(width=1063, height=768)
            root.title("I chop connections for a living")
            photo = tk.PhotoImage(file= r"../res/wolfandre.gif")
            cv = tk.Canvas()
            cv.pack(side='top', fill='both', expand='yes')
            cv.create_image(0, 0, image=photo, anchor='nw')
            root.mainloop()

            self.dto = DTOBalance()
            self.dtoListeNiveau = None
        
        
        # relatif au usager
        self.daoUser = DaoUser()
        self.users = {"player1": DTOUser(), "player2": DTOUser()}

        self.finDePartie = False
        
        self.pandaBase = pandaBase
        self.pandaBase.enableParticles()
        self.accept("DemarrerPartie",self.startGame)
        self.accept("chargerNiveau", self.startAvecNiveau)
        self.accept("cameraFinPartie",self.callFinPartie)

    def setup(self,niveau ,value):
        #DTO N'EXISTE PAS ENCORE
        #print self.dtoNiveau
        if niveau:
            id_niveau = self.dtoListeNiveau.listeNiveau[value]
            self.dtoNiveau = self.daoNiveau.read(id_niveau)
            # self.listecase = [[0 for x in range(self.dtoNiveau.dimension_x)] for y in range(self.dtoNiveau.dimension_y)]
            # for row in range(0,self.dtoNiveau.dimension_x) :
            #     for col in range(0,self.dtoNiveau.dimension_y):
            #         self.listecase[row][col] = DTOCase(row,col,1)

            self.dtoListeJoueur = self.daoListeJoueur.read(id_niveau)
            self.dtoJoueur1 = self.dtoListeJoueur.listeJoueur[0]
            self.dtoJoueur2 = self.dtoListeJoueur.listeJoueur[1]

            self.dtoListeCase = self.daoListeCase.read(id_niveau, self.dtoNiveau)
            self.listecase = self.dtoListeCase.listeCase
            

        self.setupBulletPhysics()
        self.setupCamera()
        self.setupMap()
        self.setupLightAndShadow()

        #Création d'une carte de base
        #self.carte.creerCarteParDefaut()
        if niveau:
            self.map.construireMap()
        else:
            self.map.construireMapHazard()
        
        


        #A besoin des éléments de la map
        self.setupControle()
        self.setupInterface()

        #Fonction d'optimisation
        #DOIVENT ÊTRE APPELÉE APRÈS LA CRÉATION DE LA CARTE
        #Ça va prendre les modèles qui ne bougent pas et en faire un seul gros
        #en faisant un seul gros noeud avec
        self.map.figeObjetImmobile()

        #DEBUG: Décommenter pour affiche la hiérarchie
        #self.pandaBase.startDirect()

        messenger.send("ChargementTermine")

    def startGame(self):
        niveau = False
        self.setup(niveau,None)
        #On démarrer l'effet du compte à rebour.
        #La fonction callBackDebutPartie sera appelée à la fin
        self.interfaceMessage.effectCountDownStart(self.dto.count_down_time ,self.callBackDebutPartie) # Valeur a chercher dans la BD
        self.interfaceMessage.effectMessageGeneral("Appuyer sur F1 pour l'aide",3)

    def startAvecNiveau(self,value):
        id_niveau = self.dtoListeNiveau.listeNiveau[value]
        self.dtoNiveau = self.daoNiveau.read(id_niveau)
        self.sanitizerListeCase = SanitizerListeCase(self.dtoNiveau)
        self.dtoListeCase = self.daoListeCase.read(id_niveau, self.dtoNiveau)
        self.dtoListeJoueur = self.daoListeJoueur.read(id_niveau)

        errorCase = False
        errorNiveau = False
        errorJoueur = False

        errorCase = self.sanitizerListeCase.check(self.dtoListeCase)
        errorJoueur = self.sanitizerListeJoueur.check(self.dtoListeJoueur)
        errorNiveau = self.sanitizerNiveau.check(self.dtoNiveau) 

        if errorNiveau == True or errorCase == True or errorJoueur == True:

            root = tk.Tk()
            root.withdraw()
            tkMessageBox.showwarning("Erreur selection niveau","Niveau en maintenance, acheter le DLC a 60$ (US) pour pouvoir utiliser ce niveau ! EA sports, its in the game")
            self.users = {"player1": DTOUser(), "player2": DTOUser()}
            self.daoUser = DaoUser()
            self.menuSelection = InterfaceMenuSelectionNiveau(self.dto, self.dtoListeNiveau, self.users, self.daoUser, value)
            return 

        niveau = True
        self.setup(niveau , value)
        #On démarrer l'effet du compte à rebour.
        #La fonction callBackDebutPartie sera appelée à la fin
        self.interfaceMessage.effectCountDownStart(self.dto.count_down_time ,self.callBackDebutPartie) # Valeur a chercher dans la BD
        self.interfaceMessage.effectMessageGeneral("Appuyer sur F1 pour l'aide",3)

    def setupBulletPhysics(self):
        debugNode = BulletDebugNode('Debug')
        debugNode.showWireframe(True)
        debugNode.showConstraints(True)
        debugNode.showBoundingBoxes(False)
        debugNode.showNormals(False)
        self.debugNP = render.attachNewNode(debugNode)

        self.mondePhysique = BulletWorld()
        self.mondePhysique.setGravity(Vec3(0, 0, -9.81))
        self.mondePhysique.setDebugNode(self.debugNP.node())
        taskMgr.add(self.updatePhysics, "updatePhysics")

        taskMgr.add(self.updateCarte, "updateCarte")

    def setupCamera(self):
        #On doit désactiver le contrôle par défaut de la caméra autrement on ne peut pas la positionner et l'orienter
        self.pandaBase.disableMouse()

        #Le flag pour savoir si la souris est activée ou non n'est pas accessible
        #Petit fail de Panda3D
        taskMgr.add(self.updateCamera, "updateCamera")
        self.setupTransformCamera()


    def setupTransformCamera(self):
        #Défini la position et l'orientation de la caméra
        self.positionBaseCamera = Vec3(0,-18,32)
        camera.setPos(self.positionBaseCamera)
        #On dit à la caméra de regarder l'origine (point 0,0,0)
        camera.lookAt(render)

    def setupMap(self):
        
        self.map = Map(self.mondePhysique,self.dto, self.dtoNiveau,self.dtoJoueur1,self.dtoJoueur2,self.listecase,self.dtoPartie,self.daoEnregistrement,self.users)
        #On construire la carte comme une coquille, de l'extérieur à l'intérieur
        #Décor et ciel
        self.map.construireDecor(camera)
        #Plancher de la carte
        self.map.construirePlancher()
        #Murs et éléments de la map

    def setupLightAndShadow(self):
        #Lumière du skybox
        plight = PointLight('Lumiere ponctuelle')
        plight.setColor(VBase4(1,1,1,1))
        plnp = render.attachNewNode(plight)
        plnp.setPos(0,0,0)
        camera.setLight(plnp)

        #Simule le soleil avec un angle
        dlight = DirectionalLight('Lumiere Directionnelle')
        dlight.setColor(VBase4(0.8, 0.8, 0.6, 1))
        dlight.get_lens().set_fov(75)
        dlight.get_lens().set_near_far(0.1, 60)
        dlight.get_lens().set_film_size(30,30)
        dlnp = render.attachNewNode(dlight)
        dlnp.setPos(Vec3(-2,-2,7))
        dlnp.lookAt(render)
        render.setLight(dlnp)

        #Lumière ambiante
        alight = AmbientLight('Lumiere ambiante')
        alight.setColor(VBase4(0.25, 0.25, 0.25, 1))
        alnp  = render.attachNewNode(alight)
        render.setLight(alnp)

        #Ne pas modifier la valeur 1024 sous peine d'avoir un jeu laid ou qui lag
        dlight.setShadowCaster(True, 1024,1024)
        #On doit activer l'ombre sur les modèles
        render.setShaderAuto()

    def setupControle(self,):
        #Créer le contrôle
        #A besoin de la liste de tank pour relayer correctement le contrôle
        self.inputManager = InputManager(self.map.listTank,self.debugNP,self.pandaBase)
        self.accept("initCam",self.setupTransformCamera)

    def setupInterface(self):
        self.interfaceTank = []
        self.interfaceTank.append(InterfaceTank(0,self.map.listTank[0].couleur))
        self.interfaceTank.append(InterfaceTank(1,self.map.listTank[1].couleur))

        self.interfaceMessage = InterfaceMessage(self.dto, self.dtoNiveau)

    def callBackDebutPartie(self):
        #Quand le message d'introduction est terminé, on permet aux tanks de bouger
        self.inputManager.debuterControle()

    #Mise à jour du moteur de physique
    def updateCamera(self,task):
        #On ne touche pas à la caméra si on est en mode debug
        if(self.inputManager.mouseEnabled or self.finDePartie):
            return task.cont

        vecTotal = Vec3(0,0,0)
        distanceRatio = 1.0
        if (len(self.map.listTank) != 0):
            for tank in self.map.listTank:
                vecTotal += tank.noeudPhysique.getPos()
            vecTotal = vecTotal/len(self.map.listTank)

        vecTotal.setZ(0)
        camera.setPos(vecTotal + self.positionBaseCamera)
        return task.cont

    def callFinPartie(self) :
        self.finDePartie = True
        taskMgr.add(self.updateCameraFinPartie,"updateCameraFinPartie")

    def updateCameraFinPartie(self,task) :
        self.positionBaseCamera = Vec3(0,0,30)
        camera.setPos(self.positionBaseCamera)

        # distance_du_tank = 15

        # thetime = time.time()
        # x = math.sin(thetime)*distance_du_tank
        # y = math.cos(thetime)*distance_du_tank
        # z = 0
        # camera.lookAt( Point3(0,0,0) , Vec3(x , y ,z) )

    #Mise à jour du moteur de physique
    def updatePhysics(self,task):
        dt = globalClock.getDt()
        messenger.send("appliquerForce")
        self.mondePhysique.doPhysics(dt)
        #print(len(self.mondePhysique.getManifolds()))

        #Analyse de toutes les collisions
        for entrelacement in self.mondePhysique.getManifolds():
            node0 = entrelacement.getNode0()
            node1 = entrelacement.getNode1()
            self.map.traiterCollision(node0, node1)
        return task.cont

    def updateCarte(self,task):
        # print task.time
        self.map.update(task.time)
        return task.cont
