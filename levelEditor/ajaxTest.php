<?php
    require_once("action/AjaxTestAction.php");

    $action = new AjaxTestAction();
    $action->execute();

    echo json_encode($action->result);