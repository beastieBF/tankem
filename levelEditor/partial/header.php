<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.structure.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="css/global.css" />
	
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script src="js/Tile.js"></script>
	<script src="js/Joueur.js"></script>
	<script src="js/Level.js"></script> <!-- class -->
	<!-- <script src="js/Property.js"></script>  class -->
	<script src="js/js.js"></script>

	<title> Tankem Level Editor </title>
</head>
	<body>
