<?php
    require_once("action/AjaxSaveTileAction.php");

    $action = new AjaxSaveTileAction();
    $action->execute();

    echo json_encode($action->result);