<?php
    require_once("action/IndexAction.php");

    $action = new IndexAction();
    $action->execute();

    require_once("partial/header.php");
?>
    <div class="wrapper">
        <nav>
            <h2>Menu</h2>
            <div id="accordion">
                <h3>Grid Template</h3>
                <div class="gridTemplate">
                    <label for="spinnerX">X :</label>
                    <input id="spinnerX" name="value">
                    <br>
                    <label for="spinnerY">Y :</label>
                    <input id="spinnerY" name="value">
                </div>
                <h3>Tiles</h3>
                <div id="tileContainer">
                    <label for="mur">Mur</label>
                    <div id="mur" class="tile">
                        <img src="images/mur.jpg" alt="mur">
                    </div>
                    <label for="murUp">Mur Ascendant</label>
                    <div id="murUp" class="tile">
                        <img src="images/murUp.png" alt="murUp">
                    </div>
                    <label for="murDown">Mur Descendant</label>
                    <div id="murDown" class="tile">
                        <img src="images/murDown.png" alt="murDown">
                    </div>
                    <label for="plancher">Plancher</label>
                    <div id="plancher" class="tile">
                        <img src="images/Floor.png" alt="plancher">
                    </div>
                    <label for="arbre">Arbre</label>
                    <div id="arbre" class="tile">
                        <img src="images/arbre.png" alt="arbre">
                    </div>
                    <label for="j1">Joueur 1</label>
                    <div id="j1" class="tile">
                        <img src="images/tankJ1.png" alt="Joueur 1">
                    </div>
                    <label for="j2">Joueur 2</label>
                    <div id="j2" class="tile">
                        <img src="images/tankJ2.png" alt="Joueur 2">
                    </div>
                </div>
                <h3>Sauvegarder partie</h3>
                <div>
                    Nom du niveau : <input type="text" name="levelName" id="lvlName">
                    <!-- Statut : <input type="text" name="status" id="status"> -->
                    Status:
                    <select name="status" id="status">
                        <option value="1">Actif</option>
                        <option value="2">Test</option>
                        <option value="3">Inactif</option>
                    </select> <br><br>
                    Délai minimal (sec) : <input type="number" name="delayMin" id="delayMin" value="5">
                    Délai maximal (sec) : <input type="number" name="delayMax" id="delayMax" value="10">
                    <button id="saveBtn">Sauvegarde</button>
                </div>
            </div>
        </nav>
        
        <main id="workingZone">

        </main>
    </div>

<?php
    require_once("partial/footer.php");