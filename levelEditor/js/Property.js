/*

Documentation de la bd:
this.sol:
    const 
    PLANCHER = 1,
    MUR_IMMOBILE = 2,
    MUR_DOWN = 3,
    MUR_UP = 4;

*/
class Property{

    constructor(id){
        this.nodeID = "gridItem" + id,
        this.sol = "plancher",
        this.arbre = false,
        this.player = null,
        this.move = false,
        this.startUp = false
    }

    setProperties(){
        if($(this.nodeID).className.includes("plancher arbre")) {
                this.sol = "plancher";
                this.arbre = true;
        }
        else if($(this.nodeID).className.includes("murUp arbre")) {
                this.sol = "plancher";
                this.arbre = true;
                this.move = true;
                this.startUp = true;
        }
        else if($(this.nodeID).className.includes("murDown arbre")) {
                this.sol = "plancher";
                this.arbre = true;
                this.move = true;
        }
        else if($(this.nodeID).className.includes("mur arbre")) {
                this.sol = "plancher";
                this.arbre = true;
        }
        else if($(this.nodeID).className.includes("murUp")) {
                this.sol = "plancher";
                this.move = true;
                this.startUp = true;
        }
        else if($(this.nodeID).className.includes("murDown")) {
                this.sol = "plancher";
                this.move = true;
        }
        else if($(this.nodeID).className.includes("mur")) {
                this.sol = "plancher";
        }

        if ($(this.nodeID).className.includes("player1")) {
            this.player = 1;
        }
        else if ($(this.nodeID).className.includes("player2")) {
            this.player = 2;
        }
    }
}