class Level {
    constructor (name, status = 1, dimension_x = 6, dimension_y = 6, joueur = null, tiles = [], delay_min, delay_max) {
        this.name = name,
        this.status = status,
        this.dimension_x = dimension_x,
        this.dimension_y = dimension_y,
        this.delay_min = delay_min,
        this.delay_max = delay_max,
        this.joueur = joueur,
        this.tiles = tiles
    }
}