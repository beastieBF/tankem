let property = new Array();

$("document").ready(() =>{

    // Getting node
    let spinnerXnode = document.getElementById("spinnerX");
    let spinnerYnode = document.getElementById("spinnerY");
    let workingZoneNode = document.getElementById("workingZone");
    let tileContainerNode = document.getElementById("tileContainer");
    let saveBtnNode = document.getElementById("saveBtn");

    // Variables and contantes
    const MINSIZE = 6, MAXSIZE = 12;
    let nbColumn = MINSIZE, nbRow = MINSIZE;

    // Applying UI with jquery-ui
    // Menu Accordeon
    $("#accordion").accordion({
        collapsible : true,
        heightStyle: "fill"
    });

    // Bouton Save
    saveBtnNode.onclick = saveMap;

    // Draggable Droppable tiles
    $("#tileContainer > .tile").draggable({
        revert: "invalid",
        helper: "clone",
        cursor: "move",
        containment: "document"
    });

    $("#workingZone").selectable();

    // working grid UI
    $(spinnerXnode).val(MINSIZE);
    $(spinnerYnode).val(MINSIZE);

    $(spinnerXnode).spinner({
        max: MAXSIZE,
        min: MINSIZE,
        spin: function(event, ui){
            nbColumn = ui.value;
            document.documentElement.style.setProperty('--rowNum', nbColumn);
            setGridItem(nbColumn, nbRow, property);
        }
    });

    $(spinnerYnode).spinner({
        max: MAXSIZE,
        min: MINSIZE,
        spin: function(event, ui){
            nbRow = ui.value;
            document.documentElement.style.setProperty('--colNum', nbRow);
            setGridItem(nbColumn, nbRow, property);
        }
    });
        
    // Placer la grille au chargement de la page 
    setGridItem(nbColumn, nbRow, property);
});

function save(){

    let tiles = new Array();
    let players = new Array();
    let spinnerXnode = document.getElementById("spinnerX");
    let spinnerYnode = document.getElementById("spinnerY");
    let x = 0;
    let y = $(spinnerYnode).val() - 1;

    $(".gridItem").each(function(i, obj) {

        if (obj.className.includes("plancher arbre")) {
            tiles.push(new Tile(x, y, "plancher", true, false));
        }
        else if (obj.className.includes("murUp arbre")) {
            tiles.push(new Tile(x, y, "mur", true, null, true, true));
        }
        else if (obj.className.includes("murDown arbre")) {
            tiles.push(new Tile(x, y, "mur", true, null, true, false));
        }
        else if (obj.className.includes("mur arbre")) {
            tiles.push(new Tile(x, y, "mur", true, null, false, true));
        }
        else if (obj.className.includes("murUp")) {
            tiles.push(new Tile(x, y, "mur", false, null, true, true));
        }
        else if (obj.className.includes("murDown")) {
            tiles.push(new Tile(x, y, "mur", false, null, true, false));
        }
        else if (obj.className.includes("mur")) {
            tiles.push(new Tile(x, y, "mur", false, null, false, true));
        }
        else if (obj.className.includes("plancher")) {
            tiles.push(new Tile(x, y, "plancher", false));
        }

        if (obj.className.includes("player1")) {
            tiles[i].setPlayer(1);
            players.push(new Joueur(1, x, y));
        }
        else if (obj.className.includes("player2")) {
            tiles[i].setPlayer(2);
            players.push(new Joueur(2, x, y));
        }

        tiles[i].setTileTypeID();

        x++;

        if (x == $(spinnerXnode).val()) {
            x = 0;
            y--;
        }
    });

    let level = new Level($("#lvlName").val(), parseInt($("#status").val()), parseInt($(spinnerXnode).val()), parseInt($(spinnerYnode).val()), players, tiles, parseInt($("#delayMin").val()), parseInt($("#delayMax").val()));
    console.log("Niveau : ", level);
    return level;
}

function setGridItem(nbCol, nbRow, property) {

    let workingZoneNode = document.getElementById("workingZone");
    workingZoneNode.innerHTML = null;

    for(let i = 0; i < nbCol * nbRow; i++){
        let gridItem = document.createElement("div");
        gridItem.id = "levelTile" + i;
        gridItem.className = "gridItem plancher";
        workingZoneNode.appendChild(gridItem);
    }

    $(".gridItem").droppable({
        drop: function(event, ui) {

            let count = 0;

            if (ui.draggable[0].id == "arbre") {
                $(".arbre").each(function(i, obj) {
                    count++;
                });
                count += $(".ui-selected").length;
            }

            if (event.target.className.includes("ui-selected")) {

                let errorShown = false;

                $(".ui-selected").each(function(){

                    if(this.className.includes("plancher") && ui.draggable[0].id == "arbre" && count < 5) {
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("plancher arbre");
                    }
                    else if(this.className.includes("murUp") && ui.draggable[0].id == "arbre" && count < 5) {
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("murUp arbre");
                    }
                    else if(this.className.includes("murDown") && ui.draggable[0].id == "arbre" && count < 5) {
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("murDown arbre");
                    }
                    else if(this.className.includes("mur") && ui.draggable[0].id == "arbre" && count < 5) {
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("mur arbre");
                    }
                    else if(ui.draggable[0].id == "mur"){
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("mur");
                    }
                    else if(ui.draggable[0].id == "murUp"){
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("murUp");
                    }
                    else if(ui.draggable[0].id == "murDown"){
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("murDown");
                    }
                    else if(ui.draggable[0].id == "plancher"){
                        $(this).removeClass("mur murUp murDown plancher arbre player1 player2");
                        $(this).addClass("plancher");
                    }
                    else if (ui.draggable[0].id == "j1") {
                        count = 0;
                        $(".ui-selected").each(function(i, obj) {
                            count++;
                        });
                        $(".gridItem").each(function(i, obj) {
                            if (obj.className.includes("player1")) {
                                count++;
                            }
                        });
                        if (count == 1 && event.target.className.includes("plancher") && !event.target.className.includes("arbre")) {
                            $(".ui-selected").addClass("player1");
                        }
                        else { if (!errorShown) { alert("Le joueur ne peut commencer que sur une unique case plancher"); errorShown = true; }}
                    }
                    else if (ui.draggable[0].id == "j2") {
                        count = 0;
                        $(".ui-selected").each(function(i, obj) {
                            count++;
                        });
                        $(".gridItem").each(function(i, obj) {
                            if (obj.className.includes("player2")) {
                                count++;
                            }
                        });
                        if (count == 1 && event.target.className.includes("plancher") && !event.target.className.includes("arbre")) {
                            $(".ui-selected").addClass("player2");
                        }
                        else { if (!errorShown) { alert("Le joueur ne peut commencer que sur une unique case plancher"); errorShown = true; }}
                    }
                    else { if (!errorShown) { alert("Le jeu bug avec plus que quatre arbres mon chum (lol) "); errorShown = true; }}
                });
            }
            else alert("Vous devez déposer l'icône sur la case sélectionnée");
        }
    });
}

function saveMap() {
    let level = save();
    let tile = level.tiles;
    level.tiles = null;
    if(level.joueur.length != 2){
        alert("2 joueurs doivent être présent pour sauvegarder un level")
    }
    else{
        $.ajax({
            "url": "ajaxSaveMap.php",
            "type": "POST",
            "data": {action: "saveMap" , niveau: level}
        }).done(function(data){
            console.log(data);
            data = JSON.parse(data);

            $.ajax({
                "url": "ajaxSaveTile.php",
                "type": "POST",
                "data": {
                    action: "saveTile",
                    lvlID: data,
                    tile: tile
                }
            }).done(function(data){
                console.log(data);
                data = JSON.parse(data);
                console.log(data);
                if(data == null){
                    alert("Le niveau à bien été sauvegardé");
                }
                else{
                    alert("Une erreur est survenu lors de l'enregistrement du niveau. \n Veuillez constact l'administrateur");
                }
            })
        });
    }
}