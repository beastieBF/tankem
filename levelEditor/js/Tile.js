/*
        ID NOM
---------- --------------------------------------------------
         1 Plancher
         2 AnimationMurImmobile
         3 AnimationMurVerticale
         4 AnimationMurVerticaleInverse
*/

class Tile{
    constructor(posX, posY, sol = "plancher", arbre = false, player = null, move = false, startUp = false) {
        this.posX = posX,
        this.posY = posY,
        this.sol = sol,
        this.arbre = arbre,
        this.player = player,
        this.move = move,
        this.startUp = startUp
        this.typeID;
        this.setTileTypeID();
    }

    setPlayer(noPlayer) {
        this.player = noPlayer;
    }

    setTileTypeID(){

        if(this.sol == "plancher"){
            this.typeID = 1;
        }
        else{
            if(! this.move){
                this.typeID = 2; 
            }
            else{
                if(this.startUp){
                    this.typeID = 4;
                }
                else{
                    this.typeID = 3;
                }
            }
        }
    }
}