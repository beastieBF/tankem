<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/DAO.php");

    class AjaxSaveTileAction extends CommonAction {

        public $result;

        public function __construct() {
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
        }

        protected function executeAction() {
            if($_POST["action"] === "saveTile") {
                $this->result = Dao::saveTile($_POST["lvlID"], $_POST["tile"]);

                if (strpos($this->result, "<br") !== false) {
                    $this->result = "Une erreur s'est produite";
                }
            
            }
        }
    }