<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/DAO.php");

    class AjaxSaveMapAction extends CommonAction {
        
        public $result;
        
        public function __construct() {
            parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
        }

        protected function executeAction() {
            if($_POST["action"] === "saveMap") {
                $this->result = Dao::saveMap($_POST["niveau"]);

                if (strpos($this->result, "<br") !== false) {
                    $this->result = "Une erreur s'est produite";
                }
            
            }
        }
    }