<?php

    class dtoCase {
        public  $case = array(
                "x" => 0 ,
                "y" => 0 ,
                "arbre" => FALSE,
                "type" => "Vide"
                );
        
        public function __construct($x, $y, $tree, $type) {
                $this->case["x"] = $x;
                $this->case["y"] = $y;
                $this->case["arbre"] = $tree;
                $this->case["type"] = $type;
        }
    }