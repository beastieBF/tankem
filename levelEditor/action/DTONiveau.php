<?php

    class dtoNiveau {
        public $level = array(
                "nom" => "Partie par défaut", 
                "dimension_x" => 10, 
                "dimension_y" => 10, 
                "date_creation" => "Aucune", 
                "statut" => "Actif", 
                "delaiMinimumApparition" => 3.0, 
                "delaiMaximumApparition" => 20.0,
                "case" => NULL,
                "joueur" => NULL);
        
        public function __construct($id, $name, $dimensionX, $dimensionY, $date, $status, $delaiMin, $delaiMax /*,$case = NULL, $joueur = NULL*/) {
                $this->level["id"] = $id;
                $this->level["nom"] = $name;
                $this->level["dimension_x"] = $dimensionX;
                $this->level["dimension_y"] = $dimensionY; 
                $this->level["date_creation"] = $date; 
                $this->level["statut"] = $status; 
                $this->level["delaiMinimumApparition"] = $delaiMin; 
                $this->level["delaiMaximumApparition"] = $delaiMax;
                /*$this->case = $case;
                $this->joueur = $joueur;*/
        }
    }