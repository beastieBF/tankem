<?php
	require_once("action/dao/ConnectionOracle.php");
    require_once("action/DTONiveau.php");
    require_once("action/DTOListeNiveau.php");

	class Dao {
		public static function getAllLevels() {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM TANKEM_NIVEAU");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $listeNiveau = array();

            // Comment construire une liste en php ? -- a compléter avec le DTOListeNiveau
            if ($row = $statement->fetch()) {
                
            }

            return $listeNiveau;
		}

		public static function getColName() {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='TANKEM_NIVEAU'");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $listeCol = null;

            while ($row = $statement->fetch()) {
                $listeCol[] = $row;
            }
            return $listeCol;
		}

		public static function getOneLevel($levelName) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM TANKEM_NIVEAU WHERE NOM = ?");
            $statement->bindParam(1, $levelName);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

            $niveau = NULL;

            // Meilleure facon de faire?  Scalability issue
            if ($row = $statement->fetch()) {
                $niveau = new DTONiveau($row["ID"], $row["NOM"], $row["DIMENSION_X"], $row["DIMENSION_Y"], $row["DATE_CRE"], $row["ID_STATUT"], $row["DELAI_MINIMUM_APPARITION_ITEM"], $row["DELAI_MAXIMUM_APPARITION_ITEM"]);
            }

            return $niveau;
		}

        public static function updateLevel($levelName) {
			$connection = Connection::getConnection();

			$statement = $connection->prepare("UPDATE TANKEM_NIEAU SET first_name = ? WHERE id = ?");
			$statement->bindParam(1, $user["FIRST_NAME"]);
			$statement->bindParam(2, $user["ID"]);
			$statement->execute();
		}

        public static function createLevel($levelName) {
			$connection = Connection::getConnection();

            // Meme chose éviter 50 arguments ?
			$statement = $connection->prepare("INSERT INTO TANKEM_NIVEAU() VALUES(?, ?, ?, ?, ?)");
			$statement->bindParam(1, $user["FIRST_NAME"]);
			$statement->bindParam(2, $user["ID"]);
			$statement->execute();
		}

		public static function saveTile($mapID ,$tile) {
			// Save Tiles
			$mapID = (int) $mapID;
			//echo json_encode($mapID);
			//echo json_encode($tile);
			//exit;
			$connection = Connection::getConnection();
			$updateTile = $connection->prepare("INSERT INTO TANKEM_NIVEAU_CASE VALUES(?, ?, ?, ?, ?)");
			foreach($tile as $case){
				$case["posX"] = (int) $case["posX"];
				$case["posY"] = (int) $case["posY"];
				$case["typeID"] = (int) $case["typeID"];
				$updateTile->bindParam(1, $mapID);
				$updateTile->bindParam(2, $case["posX"]);
				$updateTile->bindParam(3, $case["posY"]);
				if($case["arbre"] === "true") {
					$arbre = 1;
				}
				else {
					$arbre = 0;
				}
				$updateTile->bindParam(4, $arbre);
				$updateTile->bindParam(5, $case["typeID"]); 
				$updateTile->execute();
			}
		}

		public static function saveMap($properties) {
			$connection = Connection::getConnection();
			//echo json_encode( $properties);
			//exit;
			// Save Level
			$statement = $connection->prepare("INSERT INTO TANKEM_NIVEAU (NOM,DIMENSION_X,DIMENSION_y,ID_STATUT,DELAI_MINIMUM_APPARITION_ITEM,DELAI_MAXIMUM_APPARITION_ITEM) VALUES(?,?,?,?,?,?)");
			$properties["dimension_x"] = (int) $properties["dimension_x"];
			$properties["dimension_y"] = (int) $properties["dimension_y"];
			$properties["status"] = (int) $properties["status"];
			$properties["delay_min"] = (int) $properties["delay_min"];
			$properties["delay_max"] = (int) $properties["delay_max"];

			$statement->bindParam(1, $properties["name"]);
			$statement->bindParam(2, $properties["dimension_x"]);
			$statement->bindParam(3, $properties["dimension_y"]);
			$statement->bindParam(4, $properties["status"]);
			$statement->bindParam(5, $properties["delay_min"]);
			$statement->bindParam(6, $properties["delay_max"]);
			$statement->execute();
			
			$getLvlStatement = $connection->prepare("SELECT ID FROM TANKEM_NIVEAU WHERE NOM = ?");
			$getLvlStatement->bindParam(1, $properties["name"]);
			$getLvlStatement->execute();

			if($lvlID = $getLvlStatement->fetch()){
				// Get the new lvl ID
				$lvlID = (int)$lvlID["ID"];

				
				
				// Save Players properties
				$updatePlayer = $connection->prepare("INSERT INTO TANKEM_NIVEAU_JOUEUR VALUES(?, ?, ?, ?)");
				foreach($properties["joueur"] as $joueur) {
					$updatePlayer->bindParam(1, $lvlID);
					$joueur["noJoueur"] = (int) $joueur["noJoueur"] - 1;
					$joueur["posX"] = (int) $joueur["posX"];
					$joueur["posY"] = (int) $joueur["posY"];
					$updatePlayer->bindParam(2, $joueur["noJoueur"]);
					$updatePlayer->bindParam(3, $joueur["posX"]);
					$updatePlayer->bindParam(4, $joueur["posY"]);
					$updatePlayer->execute();
				}

			}
			return $lvlID;
		}
	}