<?php 
    require_once("action/AjaxSaveMapAction.php");

    $action = new AjaxSaveMapAction();
    $action->execute();

    echo json_encode($action->result);