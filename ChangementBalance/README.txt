***********Instructions pour modifier les valeurs des param�tres du jeu*************

1. Ex�cuter le fichier selon l'op�ration � effectuer

    1.1 oracleToCsv.bat afin d'enregistrer les donn�es contenues dans la base de donn�es dans un fichier excel ou calc.
	Ces donn�es seront modifiables et r�utilisables a l'aide du logiciel CsvToOracle.bat.

    1.2 CsvToOracle.bat pour �crire les donn�es du fichier excel ou calc dans la base de donn�es du jeu.  
        Ces donn�es seront utilis�es en param�tres dans le jeu.

2. Suivre les instructions �mises dans le programme

3. Attention, si la fen�tre libre office est ouverte lors de l'importation des donn�es de la base de don�es, le fichier CSV ne sera pas modifi�.