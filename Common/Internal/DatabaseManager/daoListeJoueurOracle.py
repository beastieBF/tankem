# coding: utf-8
from interfaceDao import *
from dtoListeJoueur import *
from dtoJoueur import *

class  DaoListeJoueurOracle(InterfaceDAO):
	def __init__(self):
		InterfaceDAO.__init__(self)
		#self.sanitizer = Sanitizer()

	def read(self,idvalue):
		#Déclaration de variable
		dto = DTOListeJoueur(idvalue)

		#Connection à la base de donnée
		connectionOracle = Connection().getConnectionOracle()

		if connectionOracle :
			# Logique pour remplir le DTO
			cur = connectionOracle.cursor()
			commande = "SELECT * FROM TANKEM_NIVEAU_JOUEUR WHERE id_niveau = :1"
			self.executerCommandeSQLAvecBinding( cur , commande , {'1': idvalue} )
			listeResultat = cur.fetchall()

			for row in listeResultat :
				dtoJoueur = DTOJoueur()
				dtoJoueur.loadOracleValue(row)
				dto.listeJoueur.append(dtoJoueur)

			cur.close()

		return dto

# Unit test
if __name__ == "__main__":
	pass
	