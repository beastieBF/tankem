# coding: utf-8

class DTOJoueur :
    def __init__(self, numero_joueur=0):
        self.id_niveau = 0
        if numero_joueur == 0:
            self.numero_joueur = 0
            self.position_x = 1
            self.position_y = 1
        else :
            self.numero_joueur = 1
            self.position_x = 8
            self.position_y = 8
            

    def loadOracleValue(self, listTuple):
        self.id_niveau = listTuple[0]
        self.numero_joueur = listTuple[1]
        self.position_x = listTuple[2]
        self.position_y = listTuple[3]
