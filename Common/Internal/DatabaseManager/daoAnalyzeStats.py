# coding: utf-8
from interfaceDao import *

class  DaoAnalyseStats(InterfaceDAO):
    def __init__(self):
        InterfaceDAO.__init__(self)
        #self.sanitizer = Sanitizer()

    def getMapNameById(self, idvalue):
        #Connection à la base de donnée
        connectionOracle = Connection().getConnectionOracle()

        if connectionOracle :
            cur = connectionOracle.cursor()
            commande = "SELECT nom FROM TANKEM_NIVEAU WHERE id = :1"
            self.executerCommandeSQLAvecBinding( cur , commande , {'1': idvalue} )
            resultat = cur.fetchall()
            resultatFinal = resultat[0]
            resultatFinal = resultatFinal[0]

            cur.close()
            return resultatFinal

    def getFavoriteMapId(self, idvalue):
        #Connection à la base de donnée
        connectionOracle = Connection().getConnectionOracle()

        if connectionOracle :
            cur = connectionOracle.cursor()
            commande = "SELECT id_niveau , COUNT(id_niveau) AS frequence FROM TANKEM_STATS_PARTIE " + \
                        "WHERE id IN (SELECT id_stats_partie FROM TANKEM_STATS_PARTIE_JOUEURS WHERE id_joueur = :1) " + \
                        "GROUP BY id_niveau ORDER BY frequence DESC"
            self.executerCommandeSQLAvecBinding( cur , commande , {'1': idvalue} )
            resultat = cur.fetchall()

            resultatFinal = resultat[0]
            resultatFinal = resultatFinal[0]

            cur.close()
            return resultatFinal

    def getWinGamePlayed(self,idvalue):
        #Connection à la base de donnée
        connectionOracle = Connection().getConnectionOracle()

        if connectionOracle :
            cur = connectionOracle.cursor()
            commande = "SELECT COUNT(id_gagnant) AS NOMBRE_GAGNER FROM TANKEM_STATS_PARTIE WHERE id_gagnant = :1"
            self.executerCommandeSQLAvecBinding( cur , commande , {'1': idvalue} )
            resultat = cur.fetchall()

            resultatFinal = resultat[0]
            resultatFinal = resultatFinal[0]

            cur.close()
            return resultatFinal
    
    def getTotalGamePlayed(self, idvalue) :
        #Connection à la base de donnée
        connectionOracle = Connection().getConnectionOracle()

        if connectionOracle :
            cur = connectionOracle.cursor()
            commande = "SELECT COUNT(id_joueur) AS NOMBRE_PARTIE FROM TANKEM_STATS_PARTIE_JOUEURS WHERE id_joueur = :1"
            self.executerCommandeSQLAvecBinding( cur , commande , {'1': idvalue} )
            resultat = cur.fetchall()

            resultatFinal = resultat[0]
            resultatFinal = resultatFinal[0]

            cur.close()
            return resultatFinal

    def getFavoriteWeapons(self, idvalue):
        #Connection à la base de donnée
        connectionOracle = Connection().getConnectionOracle()

        if connectionOracle :
            cur = connectionOracle.cursor()
            commande = "SELECT SUM(TIR_CANON) , SUM(TIR_MITRAILLETTE) , SUM(TIR_GRENADE) , SUM(TIR_SHOTGUN) , SUM(TIR_PIEGE) , SUM(TIR_MISSILE_GUIDE) " + \
            "FROM TANKEM_STATS_PARTIE_JOUEURS WHERE id_joueur = :1"
            self.executerCommandeSQLAvecBinding( cur , commande , {'1': idvalue} )
            resultat = cur.fetchall()

            resultat = resultat[0]
            tableau = [ (resultat[0],"Canon") , (resultat[1],"Mitraillette")  , (resultat[2],"Grenade") , (resultat[3],"Shotgun") , (resultat[4],"Piege") , (resultat[5],"Missile guide")]
            
            cur.close()
            return sorted(tableau, reverse=True)