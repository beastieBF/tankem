# coding: utf-8
from interfaceDao import *
from connection import *
from sanitizerBalance import *

class  DaoBalanceOracle(InterfaceDAO):
	def __init__(self):
		InterfaceDAO.__init__(self)
		self.sanitizer = SanitizerBalance()

	def read(self):
		#Déclaration de variable
		dtoBalance = DTOBalance()
		
		#Connection à la base de donnée
		connectionOracle = Connection().getConnectionOracle()
		if connectionOracle :
			# Logique pour remplir le DTO
			cur = connectionOracle.cursor()
			commande = "SELECT nom_variable,valeur_variable FROM TANKEM_DONNEE_FLOAT"
			self.executerCommandeSQL(cur,commande)
			listeResultat = cur.fetchall()
			dtoBalance.loadOracleValue(listeResultat)
			cur.close()

			cur = connectionOracle.cursor()
			commande = "SELECT nom_variable,valeur_variable FROM TANKEM_DONNEE_STRING"
			self.executerCommandeSQL(cur,commande)
			listeResultat = cur.fetchall()
			dtoBalance.loadOracleValue(listeResultat)
			cur.close()
			
			# Apply sanitizer logic
			err = self.sanitizer.check(dtoBalance)
			if err:
				dtoBalance = self.sanitizer.correct(err, dtoBalance)

		return dtoBalance	


	def update(self,dtoBalance):
		# Apply sanitizer logic
		err = self.sanitizer.check(dtoBalance)
		if err:
			dtoBalance = self.sanitizer.correct(err, dtoBalance)

		listeValeurFloat = []
		listeValeurString = []
		connectionOracle = Connection().getConnectionOracle()
		if connectionOracle :
			cur = connectionOracle.cursor()
			for key in dtoBalance.__dict__.keys() :
				if type(dtoBalance.__dict__[key]) is str :
					listeValeurString.append( ( dtoBalance.__dict__[key] , key ) )
				else :
					listeValeurFloat.append( ( dtoBalance.__dict__[key] , key ) )
			#print listeValeurFloat
			#print listeValeurString
			#Pour insertion des variables floats
			cur.bindarraysize = len(listeValeurFloat)
			cur.setinputsizes(float)
			cmd_binding = "UPDATE TANKEM_DONNEE_FLOAT SET valeur_variable = :1 WHERE nom_variable = :2"
			cur.executemany( cmd_binding , listeValeurFloat )
			connectionOracle.commit()
			cur.close()

			#Pour insertion des variables strings
			cur = connectionOracle.cursor()
			cur.bindarraysize = len(listeValeurString)
			cur.setinputsizes(0,70)
			cmd_binding = "UPDATE TANKEM_DONNEE_STRING SET valeur_variable = :1 WHERE nom_variable = :2"
			cur.executemany( cmd_binding , listeValeurString )
			connectionOracle.commit()
			cur.close()


	def executerCommandeSQL(self, curseur,commande):
		try:
			curseur.execute(commande)
		except cx_Oracle.DatabaseError as e:
			#recoit le premier elem du tuple args
			error, = e.args
			print("Erreur d'execution de commande dans cx_Oracle ")
			print(error.code)
			print(error.message)
			print(error.context)

	# Fonction qui returne vrai si la connnection est active, faux sinon
	def isConnection(self):
		connectionOracle = Connection().getConnectionOracle()
		if connectionOracle:
			print("Connection Active")
			return True
		else:
			print("Probleme de connection avec oracle")
			return False

# Unit test
if __name__ == "__main__":
	from daoBalanceOracle import *
	from dtoBalance import *
	
	dto = DTOBalance()
	print(dto.__dict__)
	daoTest = DaoBalanceOracle()
	daoTest.update(dto)

	print(daoTest.isConnection())