# coding: utf-8

class DTOListeCase :
    def __init__( self , id_niveau , dtoNiveau ):
        self.id = id_niveau
        self.dtoNiveau = dtoNiveau
        self.listeCase = [[0 for y in range(self.dtoNiveau.dimension_y)] for x in range(self.dtoNiveau.dimension_x)]