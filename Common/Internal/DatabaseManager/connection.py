import cx_Oracle
import csv
from dtoBalance import *

class Connection(): # (cx_Oracle)
    
    __instanceOracle = None
    __instanceCSV = None

    def getConnectionOracle(self):
        if Connection.__instanceOracle is None:
            try:
                Connection.__instanceOracle = cx_Oracle.Connection('e1251872', 'A', 'DECINFO')
            except cx_Oracle.DatabaseError as e:
                #recoit le premier elem du tuple args
                error, = e.args
                print("Erreur de connection a la BD cx_oracle ")
                print(error.code)
                print(error.message)
                print(error.context)

        return Connection.__instanceOracle
