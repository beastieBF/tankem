# coding: utf-8

class DTOBalance:
	def __init__(self):
		self.etat = "defaut"

		#Valeur par defaut ici
		#Caracteristique des tanks
		self.vitesse_deplacement = 10 #
		self.vitesse_rotation = 1700 #
		self.point_vie_max = 200 #
		self.temps_mouvement = 0.8 #

		#Delai des balles
		self.delai_canon = 1.2 #
		self.delai_grenade = 0.8 #
		self.delai_mitraillette = 0.4 #
		self.delai_piege = 0.8 #
		self.delai_shotgun = 1.8 #
		self.delai_guide = 3.0 #
		self.delai_spring = 0.5 #

		#Vitesse des balles
		self.vitesse_balle_canon = 14 #
		self.vitesse_balle_mitraillette = 18 #
		self.vitesse_init_balle_grenade = 16 #
		self.vitesse_balle_shotgun = 13 #
		self.vitesse_balle_piege = 4 #
		self.vitesse_init_balle_guide = 30 #
		self.vitesse_init_spring = 15 #

		#Ouverture du fusils
		self.ouverture_fusil_shotgun = 0.4 #

		#Explosion des balles
		self.grosseur_explosion_balles = 10.0 #

		#Valeur des messages
		self.welcome_message = "Tankem!" #
		self.game_start_message = "Tankem!" #
		self.game_over_message = "Fin de partie" #A FINIR AVEC LA FONCTION
		self.count_down_time = 3 #
		self.welcome_message_time = 3 #

	def endGameMessageCreation(self,joueurGagnant) :
		self.game_over_message = "Joueur "+ str(joueurGagnant) + " a gagné!"

	def loadOracle(self):
		self.etat="Oracle"

	def loadCSV(self):
		self.etat="CSV"

	def loadOracleValue(self, listTuple):
		for keyValuePair in listTuple:
			self.__dict__[keyValuePair[0]] = keyValuePair[1]