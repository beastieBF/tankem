# coding: utf-8

class DTOUser:
    def __init__(self):
        # Relative to user infos
        self.idUser = None
        self.username = ""
        self.nom = ""
        self.prenom = ""
        self.email = ""
        self.token = ""

        # Relative to game
        self.couleurTank = 0
        self.qualificatifA = ""
        self.qualificatifB = "" 
        self.hp = 0
        self.force = 0
        self.agilete = 0
        self.dexterite = 0
        self.niveau = 1
        self.experience = 0
        self.nextLevel = 0
        self.winRate = 0
        self.bestMap = ""
        self.arme1 = ""
        self.arme2 = ""
        self.point_attribut = 0

        '''
            , FAVORITE_MAP VARCHAR2(40) DEFAULT 'Aucune'
, ARME1 VARCHAR2(40) DEFAULT 'Aucune'
, ARME2 VARCHAR2(40) DEFAULT 'Aucune'
, PT_ATTRIBUT NUMBER(4) DEFAULT 0
        '''

    def getNomCalcule(self):
        nom = self.username
        
        if self.qualificatifA:
            nom += " " + self.qualificatifA
        
        if self.qualificatifB:
            nom += " " + self.qualificatifB
        
        return nom