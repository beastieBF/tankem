# coding: utf-8

class DTOJoueurPartie :
    def __init__(self, idJoueur):
        self.id_stats_partie = 0
        self.id_joueur = idJoueur
        self.experience = 0
        self.tir_canon = 0
        self.tir_mitraillette = 0
        self.tir_grenade = 0
        self.tir_shotgun = 0
        self.tir_piege = 0
        self.tir_missile_guide = 0

    def loadOracleValue(self, listTuple):
        self.id_stats_partie = listTuple[0]
        self.id_joueur = listTuple[1]
        self.experience = listTuple[2]
        self.tir_canon = listTuple[3]
        self.tir_mitraillette = listTuple[4]
        self.tir_grenade = listTuple[5]
        self.tir_shotgun = listTuple[6]
        self.tir_piege = listTuple[7]
        self.tir_missile_guide = listTuple[8]