# coding: utf-8

from dtoBalance import *

class SanitizerBalance(object):
	def __init__(self):
		# Create 2 dictionnaries to control tank min and max values
		self.minValue = dict(
			# Caracteristique des tanks
			vitesse_deplacement = 4,
			vitesse_rotation = 1000.0,
			point_vie_max = 100.0,
			temps_mouvement = 0.2,
	
			#Delai des balles
			delai_canon = 0.2,
			delai_grenade = 0.2,
			delai_mitraillette = 0.2,
			delai_piege = 0.2,
			delai_shotgun = 0.2,
			delai_guide = 0.2,
			delai_spring = 0.2,

			#Vitesse des balles
			vitesse_balle_canon = 4.0,
			vitesse_balle_mitraillette = 4.0,
			vitesse_init_balle_grenade = 10.0,
			vitesse_balle_shotgun = 4.0,
			vitesse_balle_piege = 0.2,
			vitesse_initiale_balle_guide = 20.0,
			vitesse_initiale_spring = 6.0,

			#Ouverture du fusils
			ouverture_fusil_shotgun = 0.1,

			#Explosion des balles
			grosseur_explosion_balles = 1.0,

			#Valeur des messages
			count_down_time = 0.0,
			welcome_message_time = 1.0)
		
		self.maxValue = dict(
			# Caracteristique des tanks
			vitesse_deplacement = 12,
			vitesse_rotation = 2000.0,
			point_vie_max = 2000.0,
			temps_mouvement = 2.0,
			
			#Delai des balles
			delai_canon = 10.0,
			delai_grenade = 10.0,
			delai_mitraillette = 10.0,
			delai_piege = 10.0,
			delai_shotgun = 10.0,
			delai_guide = 10.0,
			delai_spring = 10.0,

			#Vitesse des balles
			vitesse_balle_canon = 30.0,
			vitesse_balle_mitraillette = 30.0,
			vitesse_init_balle_grenade = 25.0,
			vitesse_balle_shotgun = 30.0,
			vitesse_balle_piege = 4.0,
			vitesse_initiale_balle_guide = 40.0,
			vitesse_initiale_spring = 20.0,

			#Ouverture du fusils
			ouverture_fusil_shotgun = 1.5,

			#Explosion des balles
			grosseur_explosion_balles = 30.0,

			#Valeur des messages
			count_down_time = 10.0,
			welcome_message_time = 10.0)

	def check(self, dto):
		error = ()
		intersectKey = set(self.maxValue.keys()).intersection(dto.__dict__.keys())
		for key in intersectKey:
			if dto.__dict__[key] < self.minValue[key] or dto.__dict__[key] > self.maxValue[key]:
				error += (key,)
		return error

	def correct(self, error, dto):
		dtoDefault = DTOBalance()
		for key in error:
			dto.__dict__[key] = dtoDefault.__dict__[key]
		return dto

# Unit test
if __name__ == '__main__':
	from dtoBalance import *
	dto = DTOBalance()
	sani = Sanitizer()
	error = sani.check(dto)
	if error:
		dto = sani.correct(error, dto)
