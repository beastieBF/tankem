# coding: utf-8

from daoBalanceOracle import *
from daoBalanceCSV import *

daoOracle = DaoBalanceOracle()
daoCSV = DaoBalanceCSV()

filePath = daoCSV.choisirFichierCSV("open")

if filePath:
    dto = daoCSV.read(filePath)
    if(daoOracle.isConnection()):
        daoOracle.update(dto)
        print("Edition de la BD")
    else:
        print("Erreur de connection à la BD")
        print("Veillez contecter l'administrateur system")
else:
    print("Aucun fichier CSV donnee en entre.")
    print("La base de donnee ne sera pas modifier.")