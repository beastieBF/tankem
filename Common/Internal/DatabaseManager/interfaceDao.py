# coding: utf-8
from connection import *

class  InterfaceDAO:
	def __init__(self):
		pass

	def read(self):
		pass

	def create(self):
		pass

	def update(self):
		pass

	def delete(self):
		pass

	def executerCommandeSQL(self, curseur,commande):
		try:
			curseur.execute(commande)
		except cx_Oracle.DatabaseError as e:
			#recoit le premier elem du tuple args
			error, = e.args
			print("Erreur d'execution de commande dans cx_Oracle ")
			print(error.code)
			print(error.message)
			print(error.context)

	def executerCommandeSQLAvecBinding(self, curseur , commande, parametre):	
		try:
			curseur.prepare(commande)
			curseur.execute(None, parametre)
		except cx_Oracle.DatabaseError as e:
			#recoit le premier elem du tuple args
			error, = e.args
			print("Erreur d'execution de commande dans cx_Oracle ")
			print(error)
			print(error.message)
			print(error.context)

	# Fonction qui retourne vrai si la connnection est active, faux sinon
	def isConnection(self):
		connectionOracle = Connection().getConnectionOracle()
		if connectionOracle:
			return True
		else:
			print("Probleme de connection avec oracle")
			return False

	def databaseIntegrityCheck(self):
		error = False

		connectionOracle = Connection().getConnectionOracle()
		cur = connectionOracle.cursor()
		commande = "SELECT COUNT(*) FROM all_objects WHERE object_type in ('TABLE','VIEW') AND object_name = :verification "

		#Tables Balance
		self.executerCommandeSQLAvecBinding( cur , commande , {'verification': 'TANKEM_DONNEE_FLOAT'} )
		listeResultat = cur.fetchall()
		if listeResultat[0][0] != 1 :
			error = True

		self.executerCommandeSQLAvecBinding( cur , commande , {'verification': 'TANKEM_DONNEE_STRING'} )
		listeResultat = cur.fetchall()
		if listeResultat[0][0] != 1 :
			error = True

		#Tables Niveau
		self.executerCommandeSQLAvecBinding( cur , commande , {'verification': 'TANKEM_NIVEAU'} )
		listeResultat = cur.fetchall()
		if listeResultat[0][0] != 1 :
			error = True

		self.executerCommandeSQLAvecBinding( cur , commande , {'verification': 'TANKEM_NIVEAU_CASE'} )
		listeResultat = cur.fetchall()
		if listeResultat[0][0] != 1 :
			error = True

		self.executerCommandeSQLAvecBinding( cur , commande , {'verification': 'TANKEM_NIVEAU_JOUEUR'} )
		listeResultat = cur.fetchall()
		if listeResultat[0][0] != 1 :
			error = True

		self.executerCommandeSQLAvecBinding( cur , commande , {'verification': 'TANKEM_NIVEAU_STATUT'} )
		listeResultat = cur.fetchall()
		if listeResultat[0][0] != 1 :
			error = True

		self.executerCommandeSQLAvecBinding( cur , commande , {'verification': 'TANKEM_NIVEAU_TYPE_CASE'} )
		listeResultat = cur.fetchall()
		if listeResultat[0][0] != 1 :
			error = True

		return error
