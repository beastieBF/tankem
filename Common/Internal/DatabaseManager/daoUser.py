# coding: utf-8
import bcrypt
from interfaceDao import *
from connection import *
from dtoUser import *

class DaoUser(InterfaceDAO):
    def __init__(self):
        InterfaceDAO.__init__(self)

    def authenticate(self, username, password):
        # Connection BD
        connectionOracle = Connection().getConnectionOracle()
        
        if connectionOracle:
            query = "SELECT PASSWORD FROM TANKEM_USAGER WHERE USERNAME = :1"
            listParametre = {"1": username}

            cur = connectionOracle.cursor()
            self.executerCommandeSQLAvecBinding(cur, query, listParametre)
            bdPasswd = cur.fetchall()
            cur.close()

            if not bdPasswd:
                return "MAUVAIS NOM D'USAGER (N'oublier pas de peser sur ENTER sur le champs "+"Nom d'usager"+")"
            else:
                bdPasswd, = bdPasswd[0]

            # String replace to fit this version of bcrypt
            bdPasswd = bdPasswd[:2] + 'a' + bdPasswd[3:]
            # validation
            cryptedPasswd = bcrypt.hashpw(password, bdPasswd)

            cryptedPasswd = cryptedPasswd[:2] + 'a' + cryptedPasswd[3:]

            if bdPasswd == cryptedPasswd:
                return True
            else:
                return "MAUVAIS MOT DE PASSE"

        return False

    def updatePasswd(self, username, passwd):
        connectionOracle = Connection().getConnectionOracle()
        
        if connectionOracle:
            newPasswd = bcrypt.hashpw(passwd, bcrypt.gensalt())
            
            params = {"1": newPasswd, "2": username}
            query = "UPDATE TANKEM_USAGER SET PASSWORD = :1 WHERE USERNAME = :2"

            cur = connectionOracle.cursor()
            self.executerCommandeSQLAvecBinding(cur, query, params)
            connectionOracle.commit()
            cur.close()

    def getPlayer(self, player):
        # Creation d'un usager vide
        user = DTOUser()

        # Connection BD
        connectionOracle = Connection().getConnectionOracle()
        
        if connectionOracle:
            query = "SELECT * FROM TANKEM_USAGER WHERE USERNAME = :1"
            listParametre = {"1": player}

            cur = connectionOracle.cursor()
            self.executerCommandeSQLAvecBinding(cur, query, listParametre)
            result = cur.fetchall()
            cur.close()
            user.idUser = result[0][0]
            user.niveau = result[0][1]
            user.couleurTank = result[0][2]
            user.nom = result[0][3]
            user.prenom = result[0][4]
            user.email = result[0][5]
            user.qualificatifA = result[0][6]
            user.qualificatifB = result[0][7]
            user.experience = result[0][8]
            user.username = result[0][9]
            user.point_attribut = result[0][10]
            user.hp = result[0][11]
            user.force = result[0][12]
            user.dexterite = result[0][13]
            user.agilete = result[0][15]
            user.token = result[0][16]
            user.winRate = result[0][17]
            user.bestMap = result[0][18]
            user.arme1 = result[0][19]
            user.arme2 = result[0][20]
            
            return user


    def updatePlayerEndGame(self, dtoUsager):
        # Connection BD
        connectionOracle = Connection().getConnectionOracle()
        
        if connectionOracle:
            query = "UPDATE TANKEM_USAGER SET " + \
            "niveau = :1"  + \
            ", experience = :2" + \
            ", point_attribut = :3" + \
            ", win_rate = :4" + \
            ", favorite_map = :5" + \
            ", arme1 = :6" + \
            ", arme2 = :7" + \
            " WHERE id = :8"

            listParametre = {"1": dtoUsager.niveau
            , "2": dtoUsager.experience
            , "3": dtoUsager.point_attribut
            , "4": dtoUsager.winRate
            , "5": dtoUsager.bestMap
            , "6": dtoUsager.arme1
            , "7": dtoUsager.arme2
            , "8": dtoUsager.idUser}

            cur = connectionOracle.cursor()
            self.executerCommandeSQLAvecBinding(cur, query, listParametre)
            cur.close()
            

if __name__ == '__main__':
    dao = DaoUser()
    #print "Result get2Player() : "
    #for p in player:
    #    print("JOUEUR " + p.getNomCalcule())
    #    print p.__dict__

    usernameAndPassword = "cryptedUser"
    dao.updatePasswd(usernameAndPassword, usernameAndPassword)

    print dao.authenticate(usernameAndPassword, usernameAndPassword)
    print dao.authenticate(usernameAndPassword, "wrongPasswd")