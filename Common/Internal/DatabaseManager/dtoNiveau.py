# coding: utf-8

class DTONiveau :
    def __init__(self , id_niveau):
        self.id = 0
        self.nom = "Partie par default"
        self.dimension_x = 10
        self.dimension_y = 10
        self.date_creation = "Aucune"
        self.statut = "Actif"
        self.delai_minimum_apparition = 3.0
        self.delai_maximum_apparition = 20.0

    def loadOracleValue(self, listTuple):
        self.id = listTuple[0]
        self.nom = listTuple[1]
        self.dimension_x = listTuple[2]
        self.dimension_y = listTuple[3]
        self.date_creation = listTuple[4]
        self.statut = listTuple[5]
        self.delai_minimum_apparition = listTuple[6]
        self.delai_maximum_apparition = listTuple[7]
