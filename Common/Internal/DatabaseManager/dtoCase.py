# coding: utf-8

class DTOCase :
    def __init__( self , x=0 , y=0 ):
        self.id_niveau = 0
        self.position_x = x
        self.position_y = y
        self.arbre = None
        self.id_type = 1
        # Type
        # 1. Vide
        # 2. Mur
        # 3. Mur mobile
        # 4. Mur mobile inversé

    def loadOracleValue(self, listTuple):
        self.id_niveau = listTuple[0]
        self.position_x = listTuple[1]
        self.position_y = listTuple[2]
        self.arbre = listTuple[3]
        self.id_type = listTuple[4]
