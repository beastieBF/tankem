# coding: utf-8

from dtoNiveau import *

class SanitizerNiveau(object):
    def __init__(self):
        self.longueur_nom_max = 20
        self.dimension_max = 12
        self.dimension_min = 6
    
    def check(self , dto) :
        error = False
        if len( dto.nom ) > 20 :
            dto.nom = dto.nom[:self.longueur_nom_max]

        if dto.dimension_x > self.dimension_max or dto.dimension_x < self.dimension_min or dto.dimension_y > self.dimension_max or dto.dimension_y < self.dimension_min :
            error = True

        return error
        