# coding: utf-8

import csv
from dtoBalance import *
from interfaceDao import *
from sanitizerBalance import *

import os
from tkFileDialog import askopenfilename
from tkFileDialog import asksaveasfilename
from Tkinter import Tk

class DaoBalanceCSV(InterfaceDAO):
	def __init__(self):
		self.sanitizer = SanitizerBalance()
		# Variable controle est les champs qui sont des strings dans le csv
		self.controle = ("etat", "welcome_message", "game_start_message", "('welcome_message', 'Tankem!')", "game_over_message")
	
	def read(self, filePath=""):
		dto = DTOBalance()
		data = []

		# Lecture du CSV logique
		if filePath == "":
			fileName = 'dataFile.csv'
		else:
			fileName = filePath

		with open(fileName, 'rb') as csvfile:
			csvData = csv.reader(csvfile, delimiter=',', quotechar='|')
			for row in csvData:
				if row[0] in self.controle:
					dto.__dict__[row[0]] = str(row[1])
				else:
					dto.__dict__[row[0]] = float(row[1])
		
		# Sanitizer logic
		err = self.sanitizer.check(dto)
		if err:
			print("Ces champs ne sont pas inclus dans les bornes de valeur minimaux ou maximum :")
			print(err)
			print("Ces valeurs seront remplacer par les valeurs par defaut.")
			self.sanitizer.correct(err, dto)
		return dto

	def write(self, dto, filePath=""):
		# Apply sanitizer logic
		err = self.sanitizer.check(dto)
		if err:
			print("Ces champs ne sont pas inclus dans les bornes de valeur minimaux ou maximum :")
			print(err)
			print("Ces valeurs seront remplacer par les valeurs par defaut.")
			dto = self.sanitizer.correct(err, dto)
		
		# write to csv logic
		if filePath == "":
			fileName = 'dataFile.csv'
		else:
			fileName = filePath
		
		try :
			with open(fileName, 'wb') as CSVfile:
				writeFile = csv.writer(CSVfile, dialect='excel')
				for key in dto.__dict__.keys():
					writeFile.writerows([(key, dto.__dict__[key])])
		except IOError:
			print "Could not write file : " , fileName

	# tk dialog box to ask user where to open file or save file with default values
	def choisirFichierCSV(self, action="open"):
		Tk().withdraw() #enleve la fenetre par default
		home = os.path.expanduser('~')
		if action == "open":
			filename = askopenfilename(initialdir=home+'/Desktop',initialfile='BalanceTankem.csv',filetypes=[("CSV Files", ".csv"),("All files", "")], defaultextension="*.csv")
		else:
			filename = asksaveasfilename(initialdir=home+'/Desktop',initialfile='BalanceTankem.csv',filetypes=[("CSV Files", ".csv"),("All files", "")], defaultextension="*.csv")

		return filename

# Unit test
if __name__ == '__main__':
	test = DaoBalanceCSV()
	dto = DTOBalance()
	#test.write(dto)
	test.read("C:\\Users\\1059293\\Desktop\\BalanceTankem2.csv")