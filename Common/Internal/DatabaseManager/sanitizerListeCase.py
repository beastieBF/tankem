# coding: utf-8

from dtoNiveau import *
from dtoCase import *
from dtoListeCase import *

class SanitizerListeCase(object):
    def __init__(self , dtoNiveau):
        self.dtoNiveau = dtoNiveau
        pass

    def check(self , listeCase) :
        error = False

        for rangeCase in listeCase.listeCase :
            for case in rangeCase :

                if case == 0 :
                    error = True

        return error