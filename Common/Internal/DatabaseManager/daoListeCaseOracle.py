# coding: utf-8
from interfaceDao import *
from dtoListeCase import *
from dtoCase import *

class  DaoListeCaseOracle(InterfaceDAO):
	def __init__(self):
		InterfaceDAO.__init__(self)
		#self.sanitizer = Sanitizer()

	def read(self,idvalue, dtoNiveau):
		#Déclaration de variable
		dto = DTOListeCase(idvalue, dtoNiveau)

		#Connection à la base de donnée
		connectionOracle = Connection().getConnectionOracle()

		if connectionOracle :
			# Logique pour remplir le DTO
			cur = connectionOracle.cursor()
			commande = "SELECT * FROM TANKEM_NIVEAU_CASE WHERE id_niveau = :id"
			self.executerCommandeSQLAvecBinding( cur , commande , {'id': idvalue} )
			listeResultat = cur.fetchall()
			for row in listeResultat :
				dtoCase = DTOCase()
				dtoCase.loadOracleValue(row)
				dto.listeCase[dtoCase.position_x][dtoCase.position_y] = dtoCase

			cur.close()

		return dto

# Unit test
if __name__ == "__main__":
	pass
	