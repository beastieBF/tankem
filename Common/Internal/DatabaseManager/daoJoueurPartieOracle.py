# coding: utf-8
from interfaceDao import *
from dtoJoueurPartie import *

class  DaoJoueurPartieOracle(InterfaceDAO):
	def __init__(self):
		InterfaceDAO.__init__(self)
		#self.sanitizer = Sanitizer()

	def write (self, dto):
		#Connection à la base de donnée
		connectionOracle = Connection().getConnectionOracle()

		if connectionOracle :
			# Logique pour remplir la BD
			cur = connectionOracle.cursor()
			commande = "INSERT INTO TANKEM_STATS_PARTIE_JOUEURS(id_stats_partie, id_joueur, experience, tir_canon, tir_mitraillette, tir_grenade, tir_shotgun, tir_piege, tir_missile_guide) VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9)"
			self.executerCommandeSQLAvecBinding(cur, commande, {'1': dto.id_stats_partie, '2': dto.id_joueur, '3': dto.experience, '4': dto.tir_canon, '5': dto.tir_mitraillette, '6': dto.tir_grenade, '7': dto.tir_shotgun, '8': dto.tir_piege, '9': dto.tir_missile_guide})
			cur.close()


# Unit test
if __name__ == "__main__":
	pass