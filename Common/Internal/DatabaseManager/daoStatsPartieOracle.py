# coding: utf-8
from interfaceDao import *
from dtoStatsPartie import *

class  DaoStatsPartieOracle(InterfaceDAO):
	def __init__(self):
		InterfaceDAO.__init__(self)
		#self.sanitizer = Sanitizer()

	def write(self , dto):
		#Connection à la base de donnée
		connectionOracle = Connection().getConnectionOracle()
		if connectionOracle :
			# Logique pour remplir le DTO
			cur = connectionOracle.cursor()
			newest_id = cur.var(cx_Oracle.STRING)
			commande = "INSERT INTO TANKEM_STATS_PARTIE (id_niveau , id_gagnant) VALUES ( :id1 , :id2) returning id into :python_var"
			self.executerCommandeSQLAvecBinding( cur , commande , {'id1': dto.id_niveau , 'id2': dto.id_gagnant , 'python_var' : newest_id} )
			idvalue = newest_id.getvalue()
			cur.close()
			return idvalue

# Unit test
if __name__ == "__main__":
	pass
	