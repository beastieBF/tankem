# coding: utf-8
from interfaceDao import *
from connection import *
from dtoPartie import *

class DaoEnrPartie(InterfaceDAO):
    def __init__(self):
        InterfaceDAO.__init__(self)

    def getLastIdPartie(self):
        connectionOracle = Connection().getConnectionOracle()
        if connectionOracle:
            idpartie = 0
            cur = connectionOracle.cursor()
            commande = "SELECT id_partie FROM TANKEM_PARTIE"
            self.executerCommandeSQL(cur,commande)
            listeResultat = cur.fetchall()
            cur.close()
            for r in listeResultat:
                if r[0] > idpartie:
                    idpartie = r[0]
                
            return idpartie

    def deletePartie(self,idpartie):
        connectionOracle = Connection().getConnectionOracle()
        if connectionOracle:
            cur = connectionOracle.cursor()
            statement = "DELETE FROM TANKEM_PARTIE  where id_partie = :id_partie"
            cur.execute(statement, {'id_partie':idpartie})

            statement = "DELETE FROM TANKEM_ITEMS_REPLAY  where id_partie = :id_partie"
            cur.execute(statement, {'id_partie':idpartie})

            statement = "DELETE FROM TANKEM_BALLES_REPLAY  where id_partie = :id_partie"
            cur.execute(statement, {'id_partie':idpartie})

            connectionOracle.commit()

        

    def insert(self,listeEnrPartie , listeEnrBalles , listeEnrItem ,listeNomCalc):
        # dtoPartie = DTOPartie()
        connectionOracle = Connection().getConnectionOracle()
        if connectionOracle:
            rows = []
            for e in listeEnrPartie:
                for elem in e:
                    newtuple = (elem['tank_Pos_x'] , elem['tank_Pos_y'], elem['tank_Pos_z'], elem['time'] , elem['identifiant'] , elem['idPartie'] , elem['tank_Ori_x'] , elem['tank_Ori_y'], elem['tank_arme'], elem['tank_hp'], elem['res'])
                    rows.append(newtuple)
                
            cur = connectionOracle.cursor()
            cur.executemany("INSERT INTO TANKEM_PARTIE(tank_position_x,tank_position_y,tank_position_z,temps,identifiant , id_partie , tank_orientation_x , tank_orientation_y , tank_arme_utilisee, tank_hp , resolution) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11)",rows)
            
            rows = []
            for elem in listeEnrBalles:
                newtuple = (elem['position_x'] , elem['position_y'] , elem['etat'], elem['time'], elem['idPartie'])
                rows.append(newtuple)

            cur.executemany("INSERT INTO TANKEM_BALLES_REPLAY (position_x , position_y , etat , temps , id_partie) values(:1,:2,:3,:4,:5) ",rows)
           
           
            rows = []
            for elem in listeEnrItem:
                newtuple = (elem['time'] , elem['position_x'] , elem['position_y'], elem['type'] , elem['idPartie'])
                rows.append(newtuple)

            cur.executemany("INSERT INTO TANKEM_ITEMS_REPLAY (temps , position_x , position_y , type, id_partie) values(:1,:2,:3,:4,:5) ",rows)
            connectionOracle.commit()

            rows = []
            for elem in listeNomCalc:
                newtuple = (elem['nomCalc1'] , elem['nomCalc2'] , elem['idPartie'])
                rows.append(newtuple)

            cur.executemany("INSERT INTO TANKEM_INFO_PARTIE (nomcalc1,nomcalc2,id_partie) values(:1,:2,:3) ",rows)
            connectionOracle.commit()
            cur.close()

            