# coding: utf-8
from interfaceDao import *
from dtoListeNiveau import *
from connection import *

class  DaoListeNiveauOracle(InterfaceDAO):
    def __init__(self):
        InterfaceDAO.__init__(self)
        #self.sanitizer = Sanitizer()

    def readAll(self):
        #Déclaration de variable
        dto = DTOListeNiveau()

        #Connection à la base de donnée
        connectionOracle = Connection().getConnectionOracle()
        print ( connectionOracle )
        if connectionOracle :
            # Logique pour remplir le DTO
            cur = connectionOracle.cursor()
            commande = "SELECT * FROM TANKEM_NIVEAU WHERE id_statut = 1"
            #self.executerCommandeSQL(cur,commande)
            cur.execute(commande)
            listeResultat = cur.fetchall()
            for keyValuePair in listeResultat:
                dto.listeNiveau[ keyValuePair[1] ] = keyValuePair[0]
            cur.close()

        return dto

# Unit test
if __name__ == "__main__":
	pass