# coding: utf-8

from daoBalanceOracle import *
from daoBalanceCSV import *
from subprocess import Popen

daoOracle = DaoBalanceOracle()
daoCSV = DaoBalanceCSV()

if daoOracle.isConnection():
    dto = daoOracle.read()
    filePath = daoCSV.choisirFichierCSV("save")
    if filePath:
        daoCSV.write(dto, filePath)
        Popen([r'C:\\Program Files (x86)\\LibreOffice 5\\program\\scalc.exe', filePath])
    else:
        print("Aucun fichier CSV donnee en entre.")
        print("Le fichier CSV ne sera pas modifie")

else:
    print("Erreur de connection à la BD")
    print("Veillez contecter l'administrateur system")