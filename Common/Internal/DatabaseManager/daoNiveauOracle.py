# coding: utf-8
from interfaceDao import *
from dtoNiveau import *

class  DaoNiveauOracle(InterfaceDAO):
	def __init__(self):
		InterfaceDAO.__init__(self)
		#self.sanitizer = Sanitizer()

	def read(self,idvalue):
		#Déclaration de variable
		dto = DTONiveau(idvalue)
		
		#Connection à la base de donnée
		connectionOracle = Connection().getConnectionOracle()
		if connectionOracle :
			# Logique pour remplir le DTO
			cur = connectionOracle.cursor()
			commande = "SELECT * FROM TANKEM_NIVEAU WHERE id = :id"
			self.executerCommandeSQLAvecBinding( cur , commande , {'id': idvalue} )
			listeResultat = cur.fetchall()
			dto.loadOracleValue(listeResultat[0])
			cur.close()
		return dto


# Unit test
if __name__ == "__main__":
	pass
	